% export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/epics/bases/base-3.14.12.5/lib/centos7-x86_64
%
%
%  requireExec sis8300llrf sis8300llrf-setupDefaults.py ~/oloft/lion-snaphot.txt 2
%
addpath /home/eit_ess/oloft/labca_3_5_0/bin/linux-x86_64/
addpath /home/eit_ess/oloft/labca_3_5_0/bin/linux-x86_64/labca

%help labca

LLRF_SYSTEM_NAME = 'LLRF-LION';
aux_fcns

global Ts
Ts = 1 / 96.25e6;

global M_niq
global N_niq
M_niq = 4;
N_niq = 11;

dt = N_niq * Ts;

%% Define MATLAB parameters for most commonly used PVs, to avoid hanving to chang
FF_TABLE_I = 'FF-PT0-I';
FF_TABLE_Q = 'FF-PT0-Q';
FF_TABLE_X = 'FF-PT0-$';

SP_TABLE_I = 'SP-PT0-I';
SP_TABLE_Q = 'SP-PT0-Q';
SP_TABLE_X = 'SP-PT0-$';

SP_FIXED_ENABLE_I = 'PI-I-FIXEDSPEN';
SP_FIXED_ENABLE_Q = 'PI-Q-FIXEDSPEN';
SP_FIXED_ENABLE_X = 'PI-$-FIXEDSPEN';

SP_FIXED_I = 'PI-I-FIXEDSPVAL';
SP_FIXED_Q = 'PI-Q-FIXEDSPVAL';


PI_K_I = 'PI-I-GAINK';
PI_K_Q = 'PI-Q-GAINK';
PI_K_X = 'PI-$-GAINK';

PI_TSDIVTI_I = 'PI-I-GAINTSDIVTI';
PI_TSDIVTI_Q = 'PI-Q-GAINTSDIVTI';
PI_TSDIVTI_X = 'PI-$-GAINTSDIVTI';



FF_TABLESPEED = 'FF-TABLESPEED';

IQSMPL_LOOPPHASE_COMP = 'IQSMPL-ANGOFFSETVAL';

%%
% Activate the loop phase compensation
myPut('IQSMPL-ANGOFFSETEN', 'Enabled')


%%
% Enable "predistortion" to allow for adjusting output phase
myPut('VM-PREDISTEN', 'Enabled')

%%
% Seems like the tables need to be filled to allow start,
A = 0;

myPut(FF_TABLE_I, A*ones(1,10001) );
myPut(FF_TABLE_Q, A*ones(1,10001) );

myPut(SP_TABLE_I, A*ones(1,1001) );
myPut(SP_TABLE_Q, A*ones(1,1001) );

% Check that something got written to the table
% ff_tab = myGet('FF-PT0:I');
% figure(22)
% plot(ff_tab(1:10000))



%%
nominal_various_params = ...
{   FF_TABLESPEED, '1 clk cyc.'
    'PT', 0};

myPutList(nominal_various_params);

%%
nominal_vm_ctrl_params = ...
{   %'VM-INVIEN', 'Disabled'
    %'VM-INVQEN', 'Disabled'
    %'VM-SWAPIQEN', 'Disabled'
    %'VM-MAGLIMVAL', 0.95
    'VM-MAGLIMEN', 'Enabled' };

myPutList(nominal_vm_ctrl_params)

%%
nominal_sampling_params = ...
{   'AI0-DECF', 1
    'AI1-DECF', 1
    'IQSMPL-NEARIQM', 4
    'IQSMPL-NEARIQN', 11
    'IQSMPL-CAVINDELAYVAL', 19
    'IQSMPL-CAVINDELAYEN','Enabled'
    'AI-SMNM',  393216  % Means what?
    'AI-CLKS', 'SMA'};

myPutList(nominal_sampling_params)

%%
nominal_controller_params = ...
{   'PI-$-FIXEDSPEN', 'Enabled'
    'PI-$-FIXEDFFEN', 'Disabled' % Must be Disabled in order to use the FF table
    'PI-$-SATMAX', 1
    'PI-$-SATMIN', -1
    PI_K_X, 0.2
    PI_TSDIVTI_X, 0.01 };

myPutList(nominal_controller_params);
