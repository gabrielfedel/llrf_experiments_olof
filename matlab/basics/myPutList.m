% Input is a cell array where the first column contains variable names and
% the second column the values. A $-sign is expanded to both I and Q

function myPutList(value_list)
global myPut

if size(value_list, 2) ~= 2
    error('Parameter/Value cell must have two columns');
end

for k=1:size(value_list,1)
    param_name = value_list{k,1};
    param_val = value_list{k,2};
    
    if isstr(param_val)
        fprintf('Putting %s = %s\n', param_name, param_val)
    else
        fprintf('Putting %s = %.2f\n', param_name, param_val)
    end
    
    % Replaces $ with I and Q, if there are no $, the same
    % vairable will be set twice
    myPut(strrep(param_name, '$', 'I'), param_val);
    myPut(strrep(param_name, '$', 'Q'), param_val);    
end

end