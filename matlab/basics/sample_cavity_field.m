function [y_cav, y_mo, t] = sample_cavity_field(PHI_ADJ, decimation)
global myGet
global Ts
global M_niq
global N_niq


y_cav_if = myGet('AI0');
y_mo_if = myGet('AI1');

% The paths in the FPGA for the master oscillator signal and the cavity
% signal are of different lenghts, the difference is teh length of the
% delay + three cycles for a FIFO queue
if strcmp(myGet('IQSMPL-CAVINDELAYEN'), 'Enabled')
    cav_input_delay = myGet('IQSMPL-CAVINDELAYVAL') + 3;
    y_cav_if = [zeros(1, cav_input_delay) y_cav_if(1:end-cav_input_delay)];
elseif strcmp(myGet('IQSMPL-CAVINDELAYEN'), 'Disabled')
    % Do nothing
else
    error('Unknown value for IQSMPL-CAVINDELAYEN')
end

% Convert the IF signals to baseband
Ns = floor(numel(y_cav_if)/N_niq)*N_niq; % Multiple of N_niq samples are converted

niq_conversion_vect = 2/N_niq*[   sin([0:N_niq-1]*2*pi*M_niq/N_niq) + ...
                               1i*cos([0:N_niq-1]*2*pi*M_niq/N_niq)];
                           
y_cav_raw = niq_conversion_vect*reshape(y_cav_if(1:Ns),N_niq,Ns/N_niq); 
y_mo =      niq_conversion_vect*reshape(y_mo_if(1:Ns),N_niq,Ns/N_niq); 


% Modify the phase of y_cav according to y_mo and angle_comp
% TODO: Why mean of the absolute value?
y_cav = y_cav_raw ./ (y_mo / mean(abs(y_mo))) .* exp(1i*PHI_ADJ);

% Decimate the signals
y_cav = y_cav(1:decimation:end);
y_mo = y_mo(1:decimation:end);

t = (0:numel(y_cav)-1) * N_niq * Ts * decimation;
