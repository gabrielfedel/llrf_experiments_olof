function [y_cav, y_mo, t] = run_and_sample( loopphase_comp, decimation )
global runNPulses

runNPulses(3) % Two pulses are required for changes made to parameeters to take effect.
pause(0.5) % Two pulses should take at most 2 / 14 = .14 s, using 2x margin

[y_cav, y_mo, t] = sample_cavity_field(loopphase_comp, decimation);

end

