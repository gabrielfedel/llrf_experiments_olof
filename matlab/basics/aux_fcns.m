global myGet
global myPut
global myPutTab
global runNPulses

myGet = eval(['@(pv_name) lcaGet([''', LLRF_SYSTEM_NAME, ':'' pv_name])']);
myPut = eval(['@(pv_name,data) lcaPut([''', LLRF_SYSTEM_NAME, ':'' pv_name], data)']);

% Writes a table to the firmware, an extra zero as the first element
% (A fix that is required to get something to work in the firmware
% For details see Fredrik K)                              
myPutTab = eval(sprintf('@(pv_name,data) lcaPut([''%s:'' pv_name], [0 data]);' , LLRF_SYSTEM_NAME));                              



updateTables = @() eval(sprintf('@(pv_name,data) lcaPut([''%s:FF-PT0-WRTBL.PROC'' ], 1);', LLRF_SYSTEM_NAME));

setControlParams = @(K, Ts_div_Ti) myPutList({'PI-$-GAINK', K ; 'PI-$-GAINTSDIVTI', Ts_div_Ti});

setSetPoint = @(SP) myPutList({'PI-I-FIXEDSPVAL', real(SP) ; 'PI-Q-FIXEDSPVAL', imag(SP)});



% Both sets the variable and updates it in the firmware
set_PHI_ADJ = @(phi_adj) evalin('base', ['PHI_ADJ = ' num2str(phi_adj) '; myPut(IQSMPL_LOOPPHASE_COMP, PHI_ADJ);']);
    
startTR = @() lcaPut('TR-LION:SEQ1-MSGS.VAL', [double('START') zeros(1,250)] );
stopTR = @() lcaPut('TR-LION:SEQ1-MSGS.VAL', [double('STOP') zeros(1,251)] );

% In order for a change in the number of pulses it is first requried to 
% to stop the TR and then start it again
runNPulses = @(N) evalin('base', sprintf('stopTR(); lcaPut(''TR-LION:SEQ1-NITER'',%i); startTR()', N) );


setOutputRotation = @(theta) myPutList({'VM-PREDIST-RC00', cos(theta); ...
                                        'VM-PREDIST-RC01', -sin(theta); ...
                                        'VM-PREDIST-RC10', sin(theta); ...
                                        'VM-PREDIST-RC11', cos(theta)});
