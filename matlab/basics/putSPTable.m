% Puts and enables set point table
function putSPTable(spTable)
    global myPutTab
    global myPut
    myPutTab('SP-PT0-I', real(spTable));
    myPutTab('SP-PT0-Q', imag(spTable));
    
    myPut('PI-I-FIXEDSPEN', 'Disabled')
    myPut('PI-Q-FIXEDSPEN', 'Disabled')
    
    myPut('SP-PT0-WRTBL.PROC', 1)
end

