run('setup_llrf')

% Need to set the output type to FF-driven, and disable fixed SP
% for the filling table to be played correctly

%% Put some stuff into the feedforward table and the filling table
t1 = linspace(0,1,30000);
t2 = linspace(0,1,4000);


% The feedforward table is played at the same period as the controller
myPut(FF_TABLESPEED, '11 clk cyc.');

% It is important to use myPutTab when putting a table!!
A = 0.4;
B = 0.4;

myPutTab(FF_TABLE_I, A*sin(5*pi*t1) + B);
myPutTab(FF_TABLE_Q, A*sin(3*pi*t1) + B);

myPutTab(SP_TABLE_I, A*sin(4*pi*t2) + B);
myPutTab(SP_TABLE_Q, A*sin(5*pi*t2) + B);

%updateTables()
runNPulses(10)
%myPut('SP-PT0-WRTBL.PROC', 1)



myPut('PI-I-FIXEDSPEN', 'Disabled')
myPut('PI-Q-FIXEDSPEN', 'Disabled')

% Plot what's in the tables
figure(51)
clf
subplot(211)
hold on
plot(myGet(FF_TABLE_I))
plot(myGet(FF_TABLE_Q))
title('Read-back from FF-table')

subplot(212)
hold on
plot(myGet('SP-PT0-I'))
plot(myGet('SP-PT0-Q'))
plot(mod(myGet('SP-PT0-RAWTABLE-GET') / pow2(31)+1, 2)-1, 'k--')
title('Read-back from SP-table')


%% Run a pulse and look at the output

% Turn off the controller so that only the tables are played
setControlParams(0,0);

runNPulses(3)

% Wait some time to make sure the pulse has been run
pause(0.1) 

% get the caivty field signal
[y_cav, y_mo, t] = sample_cavity_field(PHI_ADJ, 1);

% Plot the data
figure(53)
set(gcf, 'Name', 'Measured Signals')
clf
plot_iq(t, y_cav)

