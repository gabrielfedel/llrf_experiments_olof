%run('setup_llrf')

% Need to set the output type to FF-driven, and disable fixed SP
% for the filling table to be played correctly

PHI_ADJ = 2.0;

myPutList({IQSMPL_LOOPPHASE_COMP, wrapToPi(PHI_ADJ)})



%% Put some stuff into the feedforward table and the filling table
t1 = linspace(0,1,30000);
t2 = linspace(0,1,4000);


% The feedforward table is played at the same period as the controller
myPut(FF_TABLESPEED, '11 clk cyc.');



myPut('PI-I-FIXEDSPEN', 'Enabled')
myPut('PI-Q-FIXEDSPEN', 'Enabled')
myPut('PI-I-FIXEDFFEN', 'Enabled')
myPut('PI-Q-FIXEDFFEN', 'Enabled')

% It is important to use myPutTab when putting a table!!

myPutTab(FF_TABLE_I, zeros(1,32000));
myPutTab(FF_TABLE_Q, zeros(1,32000));

myPutTab(SP_TABLE_I, 0.4*ones(1,4000));
myPutTab(SP_TABLE_Q, zeros(1,4000));


myPut(SP_FIXED_I, 0.3);
myPut(SP_FIXED_Q, 0);


updateTables()



% Plot what's in the tables
figure(51)
subplot(211)
hold on
plot(myGet(FF_TABLE_I))
plot(myGet(FF_TABLE_Q))
title('Read-back from FF-table')

subplot(212)
hold on
plot(myGet(SP_TABLE_I))
plot(myGet(SP_TABLE_Q))
title('Read-back from SP-table')


%% Run a pulse and look at the output

% Turn off the controller so that only the tables are played
setControlParams(0.2,0.02);
pause(0.2) 

runNPulses(3)

% Wait some time to make sure the pulse has been run
pause(0.2) 

% get the caivty field signal
[y_cav, y_mo, t] = sample_cavity_field(PHI_ADJ, 1);

% Plot the data
figure(53)
set(gcf, 'Name', 'Cavity field signal')
clf
plot_iq(t, y_cav)

figure(54)
subplot(212)
clf
plot_ap(t, y_mo)
set(gcf, 'Name', 'MO signal')