% Puts a fixed feedforward value and enables fixed FF
function putFFFixed(fixedFF)
    global myPut
    myPut('PI-I-FIXEDFFVAL', real(fixedFF));
    myPut('PI-Q-FIXEDFFVAL', imag(fixedFF));
    
    myPut('PI-I-FIXEDFFEN', 'Enabled')
    myPut('PI-Q-FIXEDFFEN', 'Enabled')
end

