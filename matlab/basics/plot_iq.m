% Plots time-series in the IQ plane, third optional parameter determines
% plot color
function plot_iq(t, y, varargin)

subplot(2,1,1)
hold on
if numel(varargin) == 1
    plot(t, real(y), varargin{1})
else
    plot(t, real(y))
end
title('Real Part')
grid on

subplot(2,1,2)
hold on
if numel(varargin) == 1
    plot(t, imag(y), varargin{1})
else
    plot(t, imag(y))
end
title('Imaginary Part')
grid on