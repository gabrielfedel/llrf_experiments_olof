LLRF_SYSTEM_NAME = 'LLRF-LION';

[mfile_folder, name, ext] = fileparts(mfilename('fullpath'));
addpath(fullfile(mfile_folder))

run('aux_fcns')
run('setup_parameters')

if ~exist('PHI_ADJ')
    set_PHI_ADJ(-1.1);
    % TODO: change calibration on startup
end


% Configure the timing receiver for LLRF (press big button)

% Turn LLRF-system to on