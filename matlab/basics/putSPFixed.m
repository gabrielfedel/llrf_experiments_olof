% Puts a fixed set point value and enables fixed set point
function putSPFixed(sp)
    global myPut
    myPut('PI-I-FIXEDSPVAL', real(sp));
    myPut('PI-Q-FIXEDSPVAL', imag(sp));
    
    myPut('PI-I-FIXEDSPEN', 'Enabled')
    myPut('PI-Q-FIXEDSPEN', 'Enabled')
end

