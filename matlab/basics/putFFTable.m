% Puts and enables feedforward table
function putFFTable(ffTable)
    global myPutTab
    global myPut
    size(ffTable)
    myPutTab('FF-PT0-I', real(ffTable));
    myPutTab('FF-PT0-Q', imag(ffTable));
    
    myPut('PI-I-FIXEDFFEN', 'Disabled')
    myPut('PI-Q-FIXEDFFEN', 'Disabled')
    
    myPut('FF-PT0-WRTBL.PROC', 1)
end

