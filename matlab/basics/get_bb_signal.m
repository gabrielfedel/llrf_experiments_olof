function y = get_bb_signal(signal_name, decimation)
global myGet
global M_niq
global N_niq


if strcmp(signal_name, 'cavity')
    y_if = myGet('AI0');
elseif strcmp(signal_name, 'mo')
    y_if = myGet('AI1');
elseif strcmp(signal_name, 'vm')
    y_if = myGet('AI2');
else
    error(['Unknown signal: ' signal_name])
end

% Convert the IF signals to baseband
Ns = floor(numel(y_if)/N_niq)*N_niq; % Multiple of N_niq samples are converted

niq_conversion_vect = 2/N_niq*exp(-1i*[0:N_niq-1]*2*pi*M_niq/N_niq);

%niq_conversion_vect = 2/N_niq*[   sin([0:N_niq-1]*2*pi*M_niq/N_niq) + ...
%                               1i*cos([0:N_niq-1]*2*pi*M_niq/N_niq)];
                           
y_full = niq_conversion_vect*reshape(y_if(1:Ns),N_niq,Ns/N_niq);
y = y_full(1:decimation:end);
