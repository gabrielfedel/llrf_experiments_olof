% Clears all tables, one less element than the full length is put due to
% that myPutTab adds a leading zero
myPutTab(SP_TABLE_I, zeros(1,4096 - 1)  );
myPutTab(SP_TABLE_Q, zeros(1,4096 - 1)  );

myPutTab(FF_TABLE_I, zeros(1,65536 - 1) );
myPutTab(FF_TABLE_Q, zeros(1,65536 - 1) );