% Very basic comparison between the real process and a very simple model of
% it

clear_all_tables
loopphase_comp = -2.36;
setControlParams(1, 0.1);

%myPutList({'IQ-SMPL:ANG-OFFSET', wrapToPi(loopphase_comp) ; })

%%
wbw = 55e3*2*pi;
Loop_Delay = 0.6e-6;
P0 = 1.4 * exp(-s*Loop_Delay) / (s / wbw + 1);

%%
%K = 1;
%Ti = 1e-5;

%K = 1.5;
%Ti = 0.3e-5;

K = 0.5;
Ti = 0.3e-5;


setControlParams(K, Ts*N_niq/Ti);
C = K + 1 / (Ti * s);


t_ff = 0.3e-3 + 0:Ts*N_niq:3e-3;
%u_ff = 0.5 * (mod(t_ff, 2e-4) > 1e-4);
u_ff = 0.5 * sin(1000*2*pi*t_ff);
myPutTab('FF-PT0:I', u_ff );

setSetPont( (0.0) ) %/ exp(1i*LOOPPHASE_COMP) )

[y_cav, y_mo, t] = run_and_sample(loopphase_comp, 2);

figure(55)
clf
plot_iq(t, y_cav)

%%
s = tf('s');

y_out = lsim(P0 / (1 + C*P0), u_ff, t_ff);

figure(29)
clf
plot(t_ff, y_out)
hold on
plot(t, y_cav, 'r')