% Visualization of what values of loop phase compensation that gives stable
% operation
s = tf('s');

clear_all_tables;

figure(28)
clf
hold on
axis equal
axis(1.2*[-1 1 -1 1])

% Set controller params
K = 0.05;
Ti = 1e-4; %1e10;

setControlParams(K, Ts*N_niq/Ti);
C = K + 1 / (Ti * s);





for lp=linspace(0,2*pi,20)
    
loopphase_comp = lp;

%myPutList({IQSMPL_LOOPPHASE_COMP, wrapToPi(loopphase_comp)})
myPutList({IQSMPL_LOOPPHASE_COMP, 0})
setOutputRotation(wrapToPi(lp));

updateTables()

setSetPoint( (0.3) )% / exp(1i * LOOPPHASE_COMP) )
%setSetPoint( (0.5) / exp(1i * LOOPPHASE_COMP) )

runNPulses(4)
pause(0.35)

[y_cav, ~, t] = sample_cavity_field(0, 1); % No compensation for loop phase

mid_pulse_inds = (t > 1e-3) & (t < 2e-3);


figure(28)
plot( complex( exp(1i*lp) * abs(y_cav(mid_pulse_inds)) ), 'o' )

plot(exp(1i*linspace(0,2*pi)))

figure(29)
clf
plot_iq(t, y_cav)

pause
end

%% Check stable operation for some specified loop phase value
clear_all_tables
loopphase_comp = -0;

myPutList({IQSMPL_LOOPPHASE_COMP, wrapToPi(loopphase_comp)})

%
t_ff = 0.3e-3 + 0:Ts*N_niq:3e-3;
myPutTab(FF_TABLE_I, 0.1 * ((t_ff > 1e-3) & (t_ff < 2e-3)) );

%setSetPoint( (0.1) ) %/ exp(1i*LOOPPHASE_COMP) )
%setSetPoint( 0.1*exp(1i*LOOPPHASE_COMP) )

[y_cav, y_mo, t] = run_and_sample(loopphase_comp, 2);

figure(55)
clf
plot_iq(t, y_cav)

%% some simulation corresponding to the first test
figure(29)
clf


Loop_Delay = 0.7e-6;
P0 = exp(-s*Loop_Delay) / (s / (50e3*2*pi) + 1);

figure(99)
margin(C*P0)

figure(29)
hold on
    
for lp=linspace(0,2*pi,50)    
    yout = step(C*P0 / (1 + C*P0*exp(1i*lp)), 1e-3);
    plot( complex( exp(1i*lp) * min( abs(yout(end)), 2) ), 'o' )    
end

%%

%putFFFixed(0.2 + 0.2i)
putFFTable((0.5 + 0.5i) * ones(1,30000))

runNPulses(2)
pause(0.4)

y_mo =      get_bb_signal('mo', 1);
y_vm_raw =      get_bb_signal('vm', 1);
y_cav_raw =      get_bb_signal('cavity', 1);

y_vm = y_vm_raw ./ (y_mo / mean(abs(y_mo)));


% Modify the phase of y_cav according to y_mo and angle_comp
% TODO: Why mean of the absolute value?
y_cav = y_cav_raw ./ (y_mo / mean(abs(y_mo)));
%

%[y_vm, y_mo, t] = sample_vm(0, 1);
figure(10)
%plot_iq(t, y_vm_raw ./ y_cav_raw)
plot_iq(t, y_vm, 'r')
plot_iq(t, y_cav, 'g--')