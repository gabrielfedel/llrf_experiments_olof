[mfile_folder, name, ext] = fileparts(mfilename('fullpath'));
addpath(fullfile(mfile_folder, '../basics'))
setup_llrf

%%

loopphase_comp = 0;
myPutList({IQSMPL_LOOPPHASE_COMP, loopphase_comp ;})
%%

setControlParams(0,0);

A_SP = 0;
A_FF_I = 0.4;

clear_all_tables
myPutTab(FF_TABLE_I, A_FF_I*ones(1,30001) );


[y_cav, y_mo, t] = run_and_sample(loopphase_comp, 2);

mid_pulse_inds = (t > 1e-3) & (t < 2e-3);
fprintf('Mean phase shift: %.3f rad , std: %.3f rad \n', mean(angle(y_cav(mid_pulse_inds))), std(angle(y_cav(mid_pulse_inds))))

figure(5)
%clf
subplot(2,3,1)
hold on
plot(t, smooth(real(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

subplot(2,3,4)
grid on
plot(t, smooth(imag(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

subplot(2,3,[2,3,5,6])
hold on
plot(real(y_cav), imag(y_cav), 'o')
plot(real(y_mo), imag(y_mo), 'ro')
axis(1*[-1 1 -1 1]);

axis square
box on
grid on