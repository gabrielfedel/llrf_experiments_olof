% Check if the field is controlled to the correct set point, a set of set
% points in a grid are examined


setControlParams(1,0.01);

for spQ=-0.8:0.2:0.8
    
for spI=-0.8:0.2:0.8

%setSetPont( (spI + 1i*spQ) / exp(1i * LOOPPHASE_COMP) )
setSetPont( (spI + 1i*spQ) )

runNPulses(2)
pause(0.3)

[y_cav, y_mo, t] = sample_cavity_field(LOOPPHASE_COMP, 10);

mid_pulse_inds = (t > 1e-3) & (t < 2e-3);

figure(28)
hold on
plot(y_cav(mid_pulse_inds))

end

end