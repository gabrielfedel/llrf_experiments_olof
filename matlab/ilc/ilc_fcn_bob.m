function u_FF_new = ilc_fcn_bob(dt, err, u_FF,time_advance)

    k = 0.4;
    errf =  filtfilt(ones(1,10)/10,1,err);

    del = round(time_advance/dt);
    errfdel = [errf(del+1:end) ;zeros(del,1)];

    N2 = 20;
    u_FF_new = filtfilt(ones(1,N2)/N2,1,u_FF + k*errfdel);

end

