warning('off','Control:analysis:LsimStartTime')
s = tf('s');

Loop_Delay = 1e-6
w_Cav = 2*pi*12e3;
%w_Cav = 2*pi*600; %Spoke
w_Kly = 2*pi*1.5e6;
w_Samp = 2*pi*5e6;
G_Cav = w_Cav/(s+w_Cav);
G_Kly = w_Kly/(s+w_Kly);
G_Samp = w_Samp/(s+w_Samp);

P = G_Cav * G_Kly * G_Samp * exp(-s*Loop_Delay);

% leta upp frekvens wd (rad/s) d?r fasen ?r -115 grader
% V?lj Ti s? att 1/Ti = w/2.5; (ger 27 graders fasf?rlust ungef?r
% dvs vi f?r ca 180-115-27 = 38 graders fasmarginal
[mag,phase,wvec] = bode(P,{2*pi*1e4,2*pi*1e6});
mag = squeeze(mag);
phase = squeeze(phase);

designphase = -118 ; % degrees

wd = interp1(phase,wvec,designphase); % closed loop bandwidth in rad/s

Ti = 4/interp1(phase,wvec,designphase);

IbwHz = 1/Ti/2/pi;
Kp = 1/abs(evalfr(P,1i*wd));
Td = 0*Ti/5; 
Tf = Ti/25;
N = 4;

w1 = 2*pi*15e3;
zeta1 = 0.2;
zeta2 = 0;

C =  Kp*(1 + 1/(s*Ti))*(1 + s*Td)/(1 + s*Td/N)/(1 + s*Tf);
%% Compute how the control signal affects the cavity output
%  and take an approximate inverse
%  Note that the time delay from klystron to cavity is not inverted at all
%  And needs to be taken care of further down

G_yu = G_Cav*G_Kly/(1+P*C);
[sys,g] = balreal(ss(1/pade(G_yu,4)/(100e-9*s + 1)^2));
F_inv = modred(sys, g<1e-2);
%%

dt = 2e-8;
t = (0:dt:500e-6)';

% Parameters for a DTL
R_L = 60448251;
w_bw = 7.44e4;
Vc = 19743000;
phi_b = -0.4451;
I_Beam_amp = 0.0625;


t_Beam_Start = 250e-6;

% (Fundamental Fourier component = 2 * DC level)
%I_B0 = 2*I_Beam_amp*exp(1i*(pi + phi_b));
I_B0 = -2*I_Beam_amp; % Only consider SISO case

Vc_over_RL = Vc / R_L;

% Not taking factor 2 for I_G into account ...
I_G0 = (-I_B0 + Vc_over_RL); 

% Normalisera inte med abs(I_G0) !!!
% Den nominella strömmen in till kaviteten är -Vc_over_RL, 
% Den nominella strömmen ut från klystronen är däremot I_G0
I_G_max = 1.25 * abs(I_G0);

%%
dt = 20e-9;
t = (0:dt:200e-6)';
t_BS = 20e-6;
y_Ref = (t > 5e-6);
%u_Ib = smooth( sin(20e3*2*pi*(t-t_BS)) .* (t > t_BS) );
u_Ib = -I_B0*(t > t_BS);
u_FF = t*0;

u_FF = I_G0*(t > 5e-6);


%dt = 1000e-9;
dt = 10e-9;
t = (0:dt:200e-6)';
t_BS = 20e-6;
y_Ref = t*0;
u_Ib = -(t > t_BS);
u_FF = t*0;



ind_t_BS = ceil((t_BS-5e-6) / dt + 1);

figure(1)
clf
plot(t, u_FF+u_Ib)

no_iter = 35;

% Matrices to save the wave forms for each iteration
err_mat = zeros(numel(t), no_iter);
y_mat = zeros(numel(t), no_iter);
u_mat = zeros(numel(t), no_iter);


for iter = 1:no_iter

% Do simulation using current feedforward
%y = lsim([P*C/(1+P*C) G_Cav*G_Kly/(1+P*C)],[y_Ref u_FF+u_Ib],t);
y = lsim([exp(-s*Loop_Delay/2)*pade(G_Cav*G_Kly/(1+P*C), 4) ...
          exp(-s*Loop_Delay)*pade(G_Cav/(1+P*C), 4)], [u_FF u_Ib],t);

err = y_Ref-y;

rms(abs(err))

err_mat(:, iter) = err;
u_mat(:, iter) = u_FF;
y_mat(:, iter) = y;


%u_FF = ilc_fcn_olof2(t, dt, err, u_FF, 2*I_G_max, ind_t_BS, F_inv);
u_FF = ilc_fcn_bob(dt, err, u_FF);

end


% Plot iterations 
iters_to_plot = [1 2 3 5 10];
figure(1)
clf
subplot(311)
hold on
plot(t,y_mat(:, iters_to_plot));
plot(t,y_Ref,'k--')
legend('y','ref')
title('y - cavity field')

subplot(312)
hold on
plot(t, err_mat(:, iters_to_plot))
title('error')

subplot(313)
hold on
plot(t, u_mat(:, iters_to_plot))
title('u')



figure(5)
hold on
plot(rms(err_mat), 'r')
set(gca, 'YLim', [0, 0.02])
title('RMS error vs iteration')
xlabel('iteration')
ylabel('rms error')
