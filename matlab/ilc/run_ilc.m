% Get time corrseponding to the feedforward table
[~, ~, t] = sample_cavity_field(PHI_ADJ, 1);

% Play the feedforward table with the same period as the PI-controller
myPut(FF_TABLESPEED, '11 clk cyc.')

myPutList( {   'PI-$-FIXEDSPEN', 'Enabled'
               'PI-$-FIXEDFFEN', 'Disabled' } )

% Define when there should be beam (only testing, so everything is during
% the feedforward table)
t_pulse_start = 500e-6;
t_pulse_end = 1300e-6;

% When the FF table should be active
t_ff_start = 300e-6;
t_ff_end = t_ff_start + 3800e-6;
ff_inds = (t >= t_ff_start) & (t <= t_ff_end); % Samples that get updated by the ILC
N_ff = nnz(ff_inds);
t_ff = Ts*N_niq*(1:N_ff); % "Local time" during the ff-table

y_ref = 0.5 * (t >= t_pulse_start) .* (t <= t_pulse_end);


myPutTab(FF_TABLE_I, 0.5*ones(1,10001) );


u_FF = 0*t_ff;
%u_FF = 0.5*(mod(t_ff, 200e-6) < 100e-6);
%u_FF = 0.58 *(t_ff > 500e-6)

%setControlParams(0.2,0.01);
setControlParams(0,0);

pause(0.2)
runNPulses(3);
pause(0.2)

for k=1:40

myPutTab(FF_TABLE_I, u_FF );

runNPulses(3);
pause(0.5)
    
[y_cav, y_mo, t] = sample_cavity_field(PHI_ADJ, 1);


figure(5)
clf
subplot(311)
hold on
plot(1e3*t, real(y_ref), 'r', 'LineWidth', 2)
plot(1e3*t, smooth(real(y_cav)), 'LineWidth', 2)
axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
xlabel('t [ms]')
title('Cavity Field (I-part)')
grid on

subplot(312)
hold on
plot(1e3*t, imag(y_ref), 'r', 'LineWidth', 2)
plot(1e3*t, smooth(imag(y_cav)), 'LineWidth', 2)
axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
xlabel('t [ms]')
title('Cavity Field (Q-part)')
grid on

subplot(313)
hold on
plot(1e3*(t_ff_start + t_ff), smooth(u_FF), 'LineWidth', 2)
axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
xlabel('t [ms]')
title('Control Signal (I-part)')
grid on





 % Update feedforward table
 y_err = y_ref(ff_inds) - real(y_cav(ff_inds));
 u_FF = u_FF + 0.2*y_err;
 N2 = 20;
 u_FF = filtfilt(ones(1,N2)/N2,1,u_FF);

%err = y_ref(ff_inds) - y_cav(ff_inds);

%C = tf(0);
%u_FF = real( ilc_fcn_butterworth(dt, real(err), real(u_FF), C, 0.3) ) + ...
%       1i*( ilc_fcn_butterworth(dt, imag(err), imag(u_FF), C, 0.3) );
   
 

%time_advance = 4e-6; 
%u_FF = ilc_fcn_bob(dt, y_err', u_FF',time_advance)';

% 
% figure(6)
% grid on
% plot(1e3*t, smooth(imag(y_cav)), 'LineWidth', 2)
% axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
% xlabel('t [ms]')
% title('Q-part of cavity field')
% grid on


drawnow

pause

end