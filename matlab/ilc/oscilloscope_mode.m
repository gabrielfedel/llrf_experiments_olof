[mfile_folder, name, ext] = fileparts(mfilename('fullpath'));
addpath(fullfile(mfile_folder, '../basics'))
setup_llrf

%%


LOOPPHASE_COMP = 3.95;
myPutList({'IQ-SMPL:ANG-OFFSET', LOOPPHASE_COMP ; 'IQ-SMPL:ANG-OFFSET-EN', 'Enabled'})
%%

setControlParams(0.5,0.01);

%setSetPont((0.25 + 0) * exp(1i*LOOPPHASE_COMP))
%setSetPont(-0.3i*exp(1i*LOOPPHASE_COMP))
setSetPont(-0+ 0.2i)


setControlParams(0.0,0.0);



A_SP = 0;
A_FF = 0.5;

myPutTab('SP-PT0:I', A_SP*ones(1,4095)  );
myPutTab('SP-PT0:Q', 0*ones(1,4095) );
myPutTab('FF-PT0:I', A_FF*ones(1,30001) );
myPutTab('FF-PT0:Q', 0*ones(1,10001) );


%pause(1.5)

runNPulses(1)
pause(0.5)

[y_cav, y_mo, t] = sample_cavity_field(LOOPPHASE_COMP, 1);

y_cav(2323)


figure(5)
%clf
subplot(2,3,1)
hold on
plot(t, smooth(real(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

subplot(2,3,4)
grid on
plot(t, smooth(imag(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

subplot(2,3,[2,3,5,6])
hold on
plot(real(y_cav), imag(y_cav), 'o')
plot(real(y_mo), imag(y_mo), 'ro')
axis(1*[-1 1 -1 1]);

axis square
box on
grid on


%%

middle_inds = (t > 1e-3) & (t < 2e-3);
cav_angle = mean(angle(y_cav(middle_inds)));

-cav_angle + LOOPPHASE_COMP


%% Other plots of interest
figure(8)
grid on
plot(t, smooth(angle(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

figure(10)
clf
subplot(2,1,1)
hold on
plot(t, smooth(real(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

subplot(2,1,2)
grid on
plot(t, smooth(imag(y_cav)))
axis([t(1) t(end) 1.1*[-1 1]])
grid on

