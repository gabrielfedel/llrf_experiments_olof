function u_FF_new = ilc_fcn_bob2(dt, err, u_FF, C)

    k = 0.4;
    errf1 =  filtfilt(ones(1,10)/10,1,err);
    
    t = (0:dt:(numel(err)-1)*dt)';
    errf2 = lsim(C, errf1, t);
    
    
    %time_shift = 2.7e-6;    
    time_shift = 4e-6;    
    
    no_elem_shift = round(time_shift/dt);
    
    
    
    errfdel = [errf2(no_elem_shift+1:end) ;zeros(no_elem_shift,1)];

%     t0 = t(end)-time_shift;
%     t1 = t(end);
%     t0_ind = numel(t)-no_elem_shift;
%     errfdel(t>t0) = interp1([t0 t1], [errfdel(t0_ind) 0], t(t>t0));
%     

    errfdel(end-no_elem_shift:end) = errfdel(end-no_elem_shift);

    N2 = round(2e-6 / dt);
    u_FF_new = filtfilt(ones(1,N2)/N2,1,u_FF + k*errfdel');    

 %u_FF_new(end-no_elem_shift);
   % u_FF_new = [u_FF_new(1:end-no_elem_shift) ;zeros(no_elem_shift,1)];    
end

