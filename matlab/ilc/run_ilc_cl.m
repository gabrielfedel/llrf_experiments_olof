close all

% Play the feedforward table with the same period as the PI-controller
myPut(FF_TABLESPEED, '11 clk cyc.')

% Define when there should be beam (only testing, so everything is during
% the feedforward table)
t_pulse_start = 500e-6;
t_pulse_end = 1300e-6;

% Set the time range When the FF table are to be active
t_ff_start = 300e-6;
t_ff_end = t_ff_start + 3800e-6;
ff_inds = find( (t >= t_ff_start) & (t <= t_ff_end) ); % Samples that get updated by the ILC
N_ff = nnz(ff_inds);
t_ff = t(ff_inds); % "Local time" during the ff-table

% Set the nominal set point for the cavity field
SP = 0.5;
y_ref = 0.5 * (t_ff >= 0.3e-3); %(t >= t_pulse_start) .* (t <= t_pulse_end);
setSetPoint( SP )

% Set controller parameters
%K = 0.3;
%Ti = 2e-6; %1e10;

K = 0.5;
Ti = 0.5e-5; %1e10;

setControlParams(K, Ts*N_niq/Ti);
C = K + 1 / (Ti * s);

% Initialize feedforward table
u_FF = 0*t_ff;


no_iters = 2;

% Matrices to save the wave forms for each iteration
err_rec = zeros(numel(ff_inds), no_iters);
u_FF_rec = zeros(numel(ff_inds), no_iters);
y_rec = zeros(numel(ff_inds), no_iters);

for iter=1:no_iters

myPutTab(FF_TABLE_I, u_FF );
pause(0.2)
[y_cav, y_mo, t] = run_and_sample(PHI_ADJ, 2);
pause(0.2)

[y_cav, y_mo, t] = run_and_sample(PHI_ADJ, 1);

% Plot results
pd = 5; %Plot decimation (reduce the number of plotted points to allow zoom)
figure(5)
clf
subplot(211)
hold on
plot(1e3*t_ff(1:pd:end), y_ref(1:pd:end), 'r', 'LineWidth', 2)
plot(1e3*t(1:pd:end), real(y_cav(1:pd:end)), 'LineWidth', 2)
axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
xlabel('t [ms]')
title('Cavity Field (I-part)')
grid on

subplot(212)
hold on
plot(1e3*(t_ff(1:pd:end)), smooth(u_FF(1:pd:end)), 'LineWidth', 2)
axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
xlabel('t [ms]')
title('Control Signal (I-part)')
grid on


figure(6)
plot_iq(t, y_cav)



% Compute the error
err = y_ref - real(y_cav(ff_inds));
% Set the error in the first and last part to zero to avoid adaption here
err(t_ff < 400e-6) = 0;
err(t_ff >= 2000e-6) = 0;

% Store data for later visualization
err_rec(:, iter) = err;
u_FF_rec(:, iter) = u_FF;
y_rec(:, iter) = y_cav(ff_inds);

% Compute and visualize errors
dist_inds = (t > 1e-3) & (t < 1.5e-3);
fprintf('Control error during beam transient %.7f %% (rms) \n', 100*rms(real(y_cav(dist_inds)) - SP) / SP)
ft_inds = (t > 1.5e-3) & (t < 2e-3);
fprintf('Control error steady state %.7f %% (rms) \n', 100*rms(real(y_cav(ft_inds)) - SP) / SP)

figure(7)
hold on
plot(1e3*t_ff(dist_inds), -err(dist_inds) + y_ref(dist_inds), 'LineWidth', 2)
plot(1e3*t_ff(dist_inds), y_ref(dist_inds), 'r', 'LineWidth', 2)


% Update feedforward table %
%u_FF = ilc_fcn_bob2(Ts*N_niq, y_err, u_FF, C);
u_FF = ilc_fcn_oloft2(Ts*N_niq, err, u_FF, C);

drawnow

pause

end



% Plot signals at certain iterations 
iters_to_plot = [1 3 5 10];
figure(1)
clf
subplot(311)
hold on
plot(t_ff,y_rec(:, iters_to_plot));
plot(t_ff,y_ref,'k--')
legend('y','ref')
title('y - cavity field')

subplot(312)
hold on
plot(t_ff, err_rec(:, iters_to_plot))
title('error')

subplot(313)
hold on
plot(t_ff, u_FF_rec(:, iters_to_plot))
title('u')


% Plot reduction of error
figure(5)
hold on
plot(rms(err_mat), 'r')
set(gca, 'YLim', [0, 0.02])
title('RMS error vs iteration')
xlabel('iteration')
ylabel('rms error')



for k=1:numel(iters_to_plot)
    
    figure(30)
    subplot(numel(iters_to_plot), 1, k)
    hold on
    plot(t_ff,real(y_rec(:, iters_to_plot(k))) / SP, 'b');
    plot(t_ff,y_ref/SP,'r--')
    axis([1e-3 2e-3 0.8 1.2])
    title(['y - iteration ' num2str(iters_to_plot(k))] )    
    
    figure(31)
    subplot(numel(iters_to_plot), 1, k)    
    plot(t_ff,real(u_FF_rec(:, iters_to_plot(k))), 'b');
    axis([1e-3 2e-3 -0.1 1])    
    title(['u_{FF} - iteration ' num2str(iters_to_plot(k))] )    
    

end
figure(30)
legend('y','ref')
xlabel('t [ms]')

saveas(30, 'ilc_y_signal', 'epsc')

figure(31)
xlabel('t [ms]')
saveas(31, 'ilc_uff_signal', 'epsc')

