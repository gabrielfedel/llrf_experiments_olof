function u_FF_new = ilc_fcn_olof2(t, dt, err, u_FF, umax, ind_t_BS, Finv)

inds_FT = ind_t_BS:numel(t);

time_shift = 0.5e-6; %5e-6;
no_elem_shift = round(time_shift / dt);

err_filt = lsim(Finv, err(inds_FT), t(inds_FT));

u_FF(inds_FT) = u_FF(inds_FT) + 0.4*err_filt(1:end);

% Fix the end
t0 = t(end)-time_shift;
t1 = t(end);
t0_ind = numel(t)-no_elem_shift;
u_FF(t>t0) = interp1([t0 t1], [u_FF(t0_ind) 0], t(t>t0));

N_MA1 = round(2e-6 / dt);
u_FF_new = filtfilt(ones(1, N_MA1)/N_MA1, 1, u_FF);
