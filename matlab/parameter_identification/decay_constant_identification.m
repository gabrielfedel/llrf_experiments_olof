%run('../basics/setup_llrf')
%%
% Turn off the controller so that only the tables are played
setControlParams(0,0);

% The feedforward table is played at the same period as the controller
myPut(FF_TABLESPEED, '11 clk cyc.');

myPutTab(FF_TABLE_I, 0.9*ones(1,10000));
myPutTab(FF_TABLE_Q, zeros(1,65000));

myPutTab(SP_TABLE_I, zeros(1,4095));
myPutTab(SP_TABLE_Q, zeros(1,4095));

% Make sure things have been written to the firmware
pause(0.5)
%%

% Run a pulse and look at the output
runNPulses(1)

% Wait some time to make sure the pulse has been run
pause(0.1)

% get the caivty field signal
[y_cav, y_mo, t] = sample_cavity_field(PHI_ADJ, 1);


%%

%t_decay_start = 3.1605e-3;
%t_decay_end = 3.167e-3 ;

t_decay_start = 3.161e-3;
t_decay_end = 3.177e-3 ;

decay_inds = find( (t>t_decay_start) & (t<t_decay_end) );

y_decay = y_cav(decay_inds)';
t_decay = (t(decay_inds) - t(decay_inds(1)))';

% Plot the decay-part
figure(54)
clf
subplot(211)
plot_iq(t, y_cav)
subplot(212)
plot(t_decay, y_decay)
%%
exp_fun = @(p, t) (p(1)) * exp(p(2) * t) + p(3);

% Comparison between fit using nonlinear least squres (lscurvefit) and for 

% p1 <-> V0Re, p2 <-> V0Im, p3 <-> alpha, p4 <-> beta
pest_nls = lsqcurvefit(exp_fun, [1.02, 5e4*(0.9 + 0.8i), 0], t_decay, y_decay);
pest_diff = (smooth( [diff(y_decay); 0], 1)' * y_decay)' / (y_decay' * y_decay) / dt;

%error_method_fit(iter, noise_ind) = abs(pest_fit(2) - gamma) / abs(gamma);
%error_method_diff(iter, noise_ind) = abs(pest_diff - gamma) / abs(gamma);


fprintf('gamma %.0f + %.0fi y0 %.3f + %.3fi\n', real(pest_nls(2)), imag(pest_nls(2)), real(pest_nls(1)), imag(pest_nls(1)))
fprintf('gamma %.0f + %.0fi\n', real(pest_diff(1)), imag(pest_diff(1)))


% \tau = 1 / \wbw = 2Q_L / w0

figure(55)
clf
hold on
plot(y_decay, 'b.')
plot(exp_fun(pest_nls, t), 'g');
plot(exp_fun([pest_nls(1) pest_diff pest_nls(3)], t), 'r');

legend('Decay data', 'NLS', 'Derivative fit')

axis(1*[-1 1 -1 1]);
grid on
axis square

set(gca, 'FontSize', 30)
title( sprintf('\\omega_{1/2} = %.3f kHz , \\Delta \\omega = %.3f kHz', ...
        real(pest_nls(2))/1e3/2/pi, imag(pest_nls(2))/1e3/2/pi) )
