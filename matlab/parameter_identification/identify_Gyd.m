angle_comp = 0;
setOutputRotation(angle_comp)

% Get time corrseponding to the feedforward table
t_ff = (0:29999)*Ts*11; %NoClkCycles;

% Play the feedforward table with the same period as the PI-controller
myPut(FF_TABLESPEED, '11 clk cyc.')

clear_all_tables;

%u_FF = 0.5*(mod(t_ff, 200e-6) < 100e-6);
%u_FF = 0.58 *(t_ff > 500e-6)


test_signal_amp = 0.3;
setControlParams(0,0)
u_FF = test_signal_amp*ones(1, numel(t_ff));

putFFTable(u_FF);

runNPulses(2);
pause(0.3)
runNPulses(1);
[y_cav, y_mo, t] = sample_cavity_field(0, 1);
G_yd_mag_normalized = rms(y_cav(t > 0.5e-3 & t < 3e-3)) / test_signal_amp

%%

K = 0%0.5;
Ti = 10;%1e-5;

putSPFixed(0)
setControlParams(K, Ts*N_niq/Ti);
%setControlParams(0,0);
%setControlParams(0,0);

NoClkCycles = N_niq;

PS = 300e-6;
T1 = 0.5e-3;
T2 = 2.5e-3; %T1 + Ts*NoClkCycles*(30000 - round(0.2e-3 / (Ts*NoClkCycles)));



t_ff = (0:29999)*Ts*NoClkCycles;
myPut(FF_TABLESPEED, [num2str(NoClkCycles) ' clk cyc.'])

test_signal_amp = 0.5;

f_vec = logspace(3, 6.3, 40);
w_vec = 2*pi*f_vec;
G_yd_mag = 0*f_vec;

G_yd_mag2 = 0*f_vec;

for f_ind=1:numel(f_vec)

f = f_vec(f_ind);

u_FF = test_signal_amp*exp(1i * f*2*pi*t_ff);
%u_FF = test_signal_amp*square(f*2*pi*t_ff);

putFFTable(u_FF);

runNPulses(3);
pause(0.3)

    
[y_cav, y_mo, t] = sample_cavity_field(0, 1);
%t = (0:numel(t)-1) * Ts * NoClkCycles;


y_cav = y_cav - sum(y_cav)/numel(y_cav);

%y_cav_resamp
y_cav_ft = y_cav(t >= T1  + PS & t <= T2  + PS);
u_FF_ft = u_FF( find(t_ff>T1,1) + (0:numel(y_cav_ft)-1) );
 
G_yd_mag2(f_ind) = sum(y_cav_ft .* conj(u_FF_ft)) * (Ts*NoClkCycles) / (T2-T1) / test_signal_amp^2;

G_yd_mag(f_ind) = rms(y_cav_ft - sum(y_cav_ft)/numel(y_cav_ft)) ./ ...
                  test_signal_amp ;

        
figure(11)
clf
plot_iq(t, y_cav)
plot_iq(t_ff+PS, u_FF)

figure(12)
clf
plot((0:numel(y_cav_ft)-1)*Ts*N_niq,  real(y_cav_ft), 'g')
hold on
plot((0:numel(y_cav_ft)-1)*Ts*N_niq,  real(u_FF_ft), 'r')
%pause

figure(13)
clf
inds = 5500:6000;
plot((0:numel(inds)-1)*Ts*N_niq, real(y_cav_ft(inds)), 'g')
hold on
plot((0:numel(inds)-1)*Ts*N_niq, real(u_FF_ft(inds)), 'r')

      
end


%%
Pnom = 1/((1.4e-6)*s + 1) * exp(-s*0.9e-6);
%Pnom = 1/(s/(2*pi*140e3) + 1) * exp(-s*1e-6);
reg = K + 1/Ti/s;
%Pnom_fr = squeeze(freqresp(Pnom, f_vec));
%PnomTd_fr = squeeze(freqresp(Pnom*exp(-3e-6*s), f_vec));

nom_fr =  squeeze(freqresp(Pnom / (1 + Pnom*reg), w_vec));
%nom_fr =  squeeze(freqresp(Pnom, w_vec));

figure(20)
subplot(211)
%loglog(f_vec, sqrt(2)*G_yd_mag / G_yd_mag_normalized, 'r')
%hold on
loglog(f_vec, abs(G_yd_mag2) / G_yd_mag_normalized, 'g--')
hold on
loglog(w_vec/2/pi, abs(nom_fr) / G_yd_mag_normalized, 'b--')
grid on
set(gca, 'YLim', [0.05, 2])

subplot(212)
semilogx(f_vec, rad2deg(unwrap(angle(G_yd_mag2))), 'g--')
hold on
semilogx(w_vec/2/pi, rad2deg(unwrap(angle(nom_fr))), 'b--')


%%
% figure(11)
% loglog(f_vec, abs(squeeze(freqresp(0.5*(1 + 1/s/(Ts*N_niq/0.01)),f_vec*pi))) , 'g')
% hold on
% loglog(f_vec, abs( (conj(P) - conj(G_yd_mag2))./(conj(P).*conj(G_yd_mag2)) ) )

%%



% 
% figure(5)
% clf
% subplot(311)
% hold on
% plot(1e3*t, real(y_ref), 'r', 'LineWidth', 2)
% plot(1e3*t, smooth(real(y_cav)), 'LineWidth', 2)
% axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
% xlabel('t [ms]')
% title('Cavity Field (I-part)')
% grid on
% 
% subplot(312)
% hold on
% plot(1e3*t, imag(y_ref), 'r', 'LineWidth', 2)
% plot(1e3*t, smooth(imag(y_cav)), 'LineWidth', 2)
% axis([1e3*t(1) 1e3*t(end) 1.1*[-1 1]])
% xlabel('t [ms]')
% title('Cavity Field (I-part)')
% grid on
% 
% subplot(313)
% hold on
% plot(1e3*t, smooth(real(u_FF)), 'LineWidth', 2)
% xlabel('t [ms]')
% title('Control Signal (I-part)')
% grid on