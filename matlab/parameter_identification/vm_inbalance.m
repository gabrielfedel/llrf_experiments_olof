clear_all_tables;

% Set controller params
setControlParams(0, 0);

t1 = 0:Ts:30000*Ts;

myPutList({IQSMPL_LOOPPHASE_COMP, 0})
setOutputRotation(wrapToPi(0));

%radius_vec = linspace(0.1, 1.1,9);
radius_vec = linspace(0.03, 0.2,4);
radius_vec = [0.4 0.65 0.9];
phi_vec = linspace(0,2*pi, 30)
vmmag_data = zeros(numel(radius_vec), numel(phi_vec));
for radius_ind=1:numel(radius_vec)
    radius = radius_vec(radius_ind);
    for phi_ind=1:numel(phi_vec) 
        phi = phi_vec(phi_ind);


        ffTable = t1*0 + radius*exp(1i*phi);

        ffTable(1:10) = 0;
        
        myPutTab(FF_TABLE_I, real(ffTable));
        myPutTab(FF_TABLE_Q, imag(ffTable));
        lcaPut('LLRF-LION:FF-PT0-WRTBL.PROC', 1)
        
        runNPulses(2)
        pause(0.3)








        y_cav_raw = get_bb_signal('cavity', 1);
        y_mo =      get_bb_signal('mo', 1);
        y_vm_raw =      get_bb_signal('vm', 1);
        
        y_vm = y_vm_raw ./ (y_mo / mean(y_mo));
        y_cav = y_cav_raw ./ (y_mo / mean(abs(y_mo)));


        [y_cav, y_mo, t] = sample_cavity_field(0, 1);
        t = (0:numel(y_cav)-1) * N_niq * Ts;

        %sigmag = abs(y_cav_raw((t>1e-3) & (t<3e-3)));
        %vmmag_data(radius_ind, phi_ind) = mean(sigmag)
        
        %sigmag = abs(y_cav_raw((t>1e-3) & (t<3e-3)));        
        %vmmag_data(radius_ind, phi_ind) = sum(sigmag)/
    end
end


%%

figure(19)
clf
plot( (vmmag_data .* exp(1i * ones(numel(radius_vec),1) * phi_vec))' )
hold on
%plot( (vmmag_data2 .* exp(1i * ones(numel(radius_vec(1:6)),1) * phi_vec))', '--')
axis square
plot(0.00 - 0.03i + 0.94*(radius_vec(1:4)' * exp(1i * phi_vec))', 'k--')
%plot(0.15*exp(1i*linspace(0,2*pi)))
%plot(0.3*exp(1i*linspace(0,2*pi)))


% Styrsignal sparad? Ga in och satt registers i firwaren.
% Vilken ar vm, se oversiktsbilder, just nu ar det 3


%%

t = (0:numel(y_vm_raw)-1) * N_niq * Ts;

ffTable = 0.5*square(2*pi*100*t1);
%ffTable = 1.1*square(2*pi*100*t1);
%ffTable = t1*0 + 0.2*exp(1i*phi);

ffTable(1:10) = 0;

myPutTab(FF_TABLE_I, real(ffTable));
myPutTab(FF_TABLE_Q, imag(ffTable));
myPut('FF-PT0-WRTBL.PROC', 1)

runNPulses(2)
pause(0.3)


y_vm_raw =      get_bb_signal('vm', 1);


figure(19)
clf
plot_iq(t, y_vm_raw)
%plot_iq(t, ffTable)


% For more efficient implementation the output could be sweeped during the
% pulses / fixed feedforward could be used.


