using PyCall # Make sure to build PyCall with python3 for ESS stuff to work

# run the setup script!!!
# ~/llrf_v2_digital/scripts/helpers/llrf_test_setup.py -f 117

# Probably also good to run pulse script


llrf = pyimport("llrf")
epics = pyimport("epics")
lowlevelhw = pyimport("lowlevhw")




#pushfirst!(PyVector(pyimport("sys")."path"), string(base_dir, "lib/hw/ess_fpga_framework/sw/python/essffw"))
#@pyimport essffw


#pushfirst!(PyVector(pyimport("sys")."path"), string(base_dir, "scripts/helpers"))

#@pyimport pyepics
# temporarily used homebrewed file
#pushfirst!(PyVector(pyimport("sys")["path"]), "/home/eit_ess/oloft/olof")
#@pyimport store_daq_channels_mod


DAQ_LENGTH = 10000


# Setup
llrf_system = llrf.LLRFSystem()
llrf_system.connect()
llrf_board = get(llrf_system, "master")

IOC_NAME = "LLRF-TIGER"

caget(pv_name) = llrf_board.ioc.caget(pv_name)
caput(pv_name, value) = llrf_board.ioc.caput(pv_name, value)


start_er() = epics.caput("TR-$IOC_NAME:SoftSeq0-Enable-Cmd", 1)
stop_er() = epics.caput("TR-$IOC_NAME:SoftSeq0-Disable-Cmd", 1)




for ch=0:7
    llrf_board.ioc.caput("DWN$ch-DAQFMT", "I/Q")
end

caput("PI-SATMIN", -2) 
caput("PI-SATMAX", 2)


function read_ctrl_input()
    y_cav_input = read_iq_signal("DWN0");
    y_phref = read_iq_signal("DWN1");

    y = y_cav_input ./ (y_phref ./ abs.(y_phref))
    return y
end






function run_pulses(sleep_time=0.2)
    start_er()
    sleep(sleep_time)
    stop_er()
end


function read_internal(SIGNAL_NAME::String, N=Nothing())    
    iq_tuples = get(llrf_board, SIGNAL_NAME)[:read]()    
    return [complex.(iq_point[1], iq_point[2]) for iq_point in iq_tuples]
    #return complex.(tmp[2,:], tmp[1,:]) # NOTE: I and Q possibly ordered differently ?!
end


function read_iq_signal(SIGNAL_NAME::String)
    err_i = llrf_board.ioc.caget("$SIGNAL_NAME-CMP0")
    err_q = llrf_board.ioc.caget("$SIGNAL_NAME-CMP1")    
    err_i + im*err_q
end


# Not quite Ki...
function set_pi_params(Kp=0, Ki=0)
    caput("PI-I-GAINK", Kp)
    caput("PI-Q-GAINK", Kp)
    caput("PI-I-GAINTSDIVTI", Ki)
    caput("PI-Q-GAINTSDIVTI", Ki)
end


function write_ff_table(u)
    caput("PI-I-FIXEDFFEN", 0)
    caput("PI-Q-FIXEDFFEN", 0)

    caput("FF-PT0-I", real(u))
    caput("FF-PT0-Q", imag(u))
    caput("FF-PT0-WRTBL.PROC", 1)
end

function write_sp_table(u)
    caput("PI-I-FIXEDSPEN", 0)
    caput("PI-Q-FIXEDSPEN", 0)

    caput("SP-PT0-I", real(u))
    caput("SP-PT0-Q", imag(u))
    caput("SP-PT0-WRTBL.PROC", 1)
end


function set_predist_mat(M)
    @assert size(M) == (2,2)

    # Disable IQ angle offset
    caput("IQSMPL-ANGOFFSETVAL", 0)
    caput("IQSMPL-ANGOFFSETEN", 0)

    # Enable predistoration matrix
    caput("VM-PREDISTEN", 1)

    caput("VM-PREDIST-RC00", M[1,1])
    caput("VM-PREDIST-RC01", M[1,2])
    caput("VM-PREDIST-RC10", M[2,1])
    caput("VM-PREDIST-RC11", M[2,2])
end


function rot_mat(θ)
    [cos(θ) -sin(θ); sin(θ) cos(θ)]
end

function get_predist_mat()
    M = Matrix{Float64}(undef, 2, 2)

    M[1,1] = caget("VM-PREDIST-RC00")
    M[1,2] = caget("VM-PREDIST-RC01")
    M[2,1] = caget("VM-PREDIST-RC10")
    M[2,2] = caget("VM-PREDIST-RC11")

    return M
end

function prepare_evr()
    div = int(88061948.02 / rate)
    runmode = 0 # runmode normal
    trgsrc = 1 # trigger source is software
    res = 3 # ns

    caput("PS0-Div-SP", div)
    caput("SoftSeq0-RunMode-Sel", runmode)
    caput("SoftSeq0-TrigSrc-2-Sel", trgsrc)
    caput("SoftSeq0-TsResolution-Sel", res)

    P = int((1.0/rate)*1000000000) # pulse size in ns

    evtcode_vec = []
    timestamp_vec = []

    for k=1:N_pulses
        newts_vec = (i*P)
        evtcode_vec += [14, 15, 16, 17]
        timestamp_vec += [newts, newts+10000, newts+310000, newts+(length*1000)+310000]
    end

    evtcode.append(127)
    timestamp.append((timestamp[-1]) + 10000)

    caput("SoftSeq0-EvtCode-SP", evtcode)
    caput("SoftSeq0-Timestamp-SP", timestamp)


end


if caget("VM-PREDISTEN") == 1 && iszero(get_predist_mat())
    warning("Zero predist matrix enabled")
end


function ddc_niq(u, M_niq, N_niq)
    Δ = 2*pi*M_niq/N_niq
    filt(ones(N_niq)/N_niq, 1, u .* exp.(-im*Δ*(0:length(u)-1)))
end

function plot_iq(t, y, dec=1)
    inds = 1:dec:length(t)
    plot(t[inds], real(y[inds]))
    plot(t[inds], imag(y[inds]))
end

function plot_iq(y; step=1, dec=1)
    t = range(0, length=length(y), step=step)
    plot_iq(t, y, dec)
end

squarewave(f, t_vec, duty=0.5) = [mod(t, 1/f) > duty/f ? 1 : -1 for t in t_vec]


function shiftsignal(x::Vector, d)
    xshifted = zeros(eltype(x), length(x))
    if d >= 0
        xshifted[1+d:end] .= x[1:end-d]
    else
        d = -d
        xshifted[1:end-d] .= x[1+d:end]
    end
    xshifted
end



#caput("PI-Q-GAINK", 0)


# Setup some parameters
M_niq = 3
N_niq = 14

f_s = caget("F-SAMPLING") * 1e6 # F-SAMPLING is in MHz !?
Ts = 1 / f_s

Ts_reg = Ts * N_niq
w_IF = 2*pi*M_niq/N_niq/Ts


llrf_board.ioc.caput("FF-TABLESPEED", N_niq - 1) # NOTE: Enum is used 0 <-> 1 clk/sample

#print(keys(llrf_board))


# Stop EVR



# Issue with the first channel
# can't seem to get to the raw data through julia

# There should be a fucntion with just force_pulse() including all of the above

# llrf_board.disconnect() is this important to remember?
