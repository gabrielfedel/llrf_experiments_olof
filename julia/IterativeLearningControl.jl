# Basic functons for iterative learning control
module IterativeLearningControl



function shift!(a::Vector, d::Int, pad_value=0)
    if d > 0
        for k=length(a):-1:d + 1
            a[k] = a[k-d]
        end
        a[1:d] .= pad_value
    else
        d = -d # D is now positive!        
        for k=1:length(a) - d
            a[k] = a[k + d]
        end
        a[end-d+1:end] .= pad_value
    end
    a
end


end