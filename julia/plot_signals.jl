ENV["MPLBACKEND"]="qt5agg"
using PyPlot

include("init_llrf.jl")

set_pi_params(0, 0)


write_sp_table(zeros(4000))


t2 = range(0, step=Ts_reg, length=25000)
write_ff_table(0.8*squarewave(2e3, t2))



run_pulses(0.5)
u_out = read_internal("IQ_AFTER_CTRL");
err = read_iq_signal("PIERRILC");
y = read_ctrl_input();

t = range(0, step=Ts_reg, length=length(y))

ax1 = subplot(3, 1, 1)
plot_iq(u_out, step=Ts_reg)

subplot(3, 1, 2, sharex=ax1)
plot_iq(-err, step=Ts_reg)

subplot(3, 1, 3, sharex=ax1)
plot_iq(y[1:length(err)], step=Ts_reg) # Hmm, looks weirdjupyter notebook list# Just to make sure that things work
plot_iq(-err, step=Ts_reg)

