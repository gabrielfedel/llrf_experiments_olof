using ControlSystems

h = 1e-7
γ = 2π*90e3
τ = 1e-6
h = 100e-9

Pd = c2d(DemoSystems.fotd(;T=1/γ, τ=τ), h)

C = pi_ss(1, 1e-4)
Cd = c2d(C, h)[1]


u = [zeros(1000); ones(1000); zeros(1000); ones(1000)]
G = feedback(Pd, Cd)

using PyPlot
lsim(G, u)
