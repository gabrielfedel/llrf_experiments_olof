# Some temporary extensions to the functionality of ControlSystems

#= delay(τ; Ts=1) = delayd(τ, Ts)

function delayd(τ, h)
    n = Int(round(τ / h))
    if !(τ - n*h ≈ 0)
        error("Currently only supported when delays are multiples of the sample time.")
    end
    ss(diagm(1 => ones(n-1)), [zeros(n-1,1); 1], [1 zeros(1,n-1)], 0, h)
end

"""
    c2d(G::DelayLtiSystem, Ts, method=:zoh)
"""
function ControlSystems.c2d(G::DelayLtiSystem, h::Real)
    X = append([delayd(τ,h) for τ in G.Tau]...)
    Pd = ControlSystems.c2d(G.P.P, h)[1]
    return lft(Pd, X)
end =#

ControlSystems.freqresp(sys::LTISystem, ω::Real) = evalfr(sys, im*ω)

function ControlSystems.ss(text::String)
    ex = Meta.parse(replace(text, "s" => """tf("s")"""))
    ss(eval(ex))
end
write_sp_table(0.01*ones(4000))
