
@time write_ff_table(0.8*[ones(10000); zeros(10000); ones(10000)])


# dwncmp0 = epics.PV("LLRF-TIGER:PIERRILC-CMPO")
# x = dwncmp0.get()

llrf_board.ioc.caput("DWN0-DAQFMT", "I/Q")

#@time get(llrf_board, "IQ_AFTER_CTRL")[:read]()


# @time x = get(llrf_board, "IQ_AFTER_CTRL")[:read_raw]()


#dwncmp0 = epics.PV("LLRF-TIGER:DWN0-CMP0")

#ilc_err = epics.PV("LLRF-TIGER:PI_ERROR_ILC")
#@time f_S = llrf_board.ioc.caget("PI_ERROR_ILC")*1e6


#epics.caget(dwncmp0, count=10)

#get(llrf_board, "IQ_AFTER_CTRL")[:read]()

##
t = range(0, length=35000, step=Ts_reg)
llrf_board.ioc.caput("DWN0-DAQFMT", "I/Q")

y_cav_input = read_iq_signal("DWN0")
y_phref = read_iq_signal("DWN1")

y_cav_input ./ (y_phref ./ abs.(y_phref))


##
ENV["MPLBACKEND"]="qt5agg"
using PyPlot
ioff()


figure(3)
#plot(randn(10000))


plot(t[1:10:5000]/1e-3, abs.(y_cav_input[1:10:5000]))

show(block=false)