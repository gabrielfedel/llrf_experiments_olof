#using ControllerDesign

using ControlSystems
using DSP
ENV["MPLBACKEND"]="qt5agg"
using PyPlot

#pygui(:qt5)

include("help_fcns.jl")
include("IterativeLearningControl.jl")
include("PyControlPlots.jl")

h = 1e-7
γ = 2π*12e3
τ = 1e-6

h = 100e-9

#H = PolynomialRatio([0.25], [1, -0.75])
#filtfilt(H, [1,0,0,0])

P = c2d(DemoSystems.fotd(;T=1/γ, τ=τ), h)

pi_params = amigo_pi_design(1/γ, τ, 1)

C = c2d(pi_ss(pi_params[1], pi_params[2], 1/(2π*2e6)), h)[1]

Ω = 2π*logspace(2, 6.3, 200)

C_fr = freqresp(C, Ω)[:]
P_fr = freqresp(P, Ω)[:]
L_fr = freqresp(P*C, Ω)[:]


loglog(Ω/2π, abs.(P_fr))
loglog(Ω/2π, abs.(C_fr))
loglog(Ω/2π, abs.(L_fr))
show(block=false)


t = 0:h:200e-6

G_yd = feedback(P, C)
G_yr = feedback(P*C, 1)




## tune..
κ = 0.5
delta = -18
Qbar = PolynomialRatio(digitalfilter(Lowpass(3.0e5, fs=1/h), Butterworth(2)))

Qbar_tf = tf(Qbar.b, Qbar.a, Discrete(h))
Qbar_fr = freqresp(Qbar_tf, Ω)[:]

G_yr_fr = freqresp(G_yr, Ω)[:]
G_yd_fr = freqresp(G_yd, Ω)[:]
L_fr = κ * exp.(-im*h*delta * Ω)




#A_fr = Qbar_fr .* conj(Qbar_fr) .* (1 .- L_fr .* G_yr_fr)

A_fr = Qbar_fr .* conj(Qbar_fr) .* (1 .- L_fr .* C_fr .* G_yd_fr)

println(maximum(abs.(A_fr)))

#A_fr = 1 .- L_fr .* G_fr 
#A_fr = Qbar_fr .* conj(Qbar_fr)
#figure(5)
#PyControlPlots.bodeplot(Ω/2π, A_fr)

show(;block=false)


##

function run_ILC(d)
    N_iters = 50

    h = G_yr.Ts

    u_ILC = zeros(size(r))

    e = zeros(length(t))

    y_hist = zeros(length(t), N_iters)
    u_hist = zeros(length(t), N_iters)

    for iter=1:N_iters
        y = lsim(G_yd, d + u_ILC, t)[1][:]

        Cy = lsim(C, y[:], t)[1][:]

        e .= -Cy[:]
        
        IterativeLearningControl.shift!(e, delta, e[end])

        u_ILC += κ * e

        u_ILC = filtfilt(Qbar, u_ILC)

        u_hist[:, iter] .= u_ILC
        y_hist[:, iter] .= y[:]
    end

    u_hist, y_hist
end


t = 0:h:100e-6

#r = [zeros(100); 0.5*(1 .+ sin.(LinRange(-π/2, π/2, 200))); ones(701) ]
r = zeros(length(t))

d = ComplexF64.(t .> 20e-6)
@time u_hist, y_hist = run_ILC(d)

figure(2)
close()
clf()
subplot(3, 1, 1)
plot(t, u_hist)

subplot(3, 1, 2)
plot(t, y_hist)
show(block=false)
#plot(t, r, "k:")

e_hist = y_hist - r * ones(1, size(u_hist,2))
subplot(3, 1, 3)
plot(t, e_hist)
show(block=false)


[norm(e_hist[:, k]) for k=1:size(e_hist, 2)]
##
