#Some syst id experiments

using Optim
using ControlSystems

ENV["MPLBACKEND"]="qt5agg"
using PyPlot 



N_samples = 12000

N_signal = N_niq*2000
t_sig = Ts_reg*(0:N_signal)

#u0 = 0.5*squarewave(1.5e4, t)

llrf_board.ioc.caput("FF-TABLESPEED", N_niq - 1)

set_pi_params()

#write_sp_table(zeros(4000))
write_sp_table(0.0*ones(4000))
#write_ff_table([0; w; zeros(100)])
w = [0.3*squarewave(2e3, t_sig); zeros(1000)]

write_ff_table(w)



# When is the pulse turned on?!

#run_pulses(4)

#run(`bash /iocs/sis8300llrf/tr-sequencer.sh TR LLRF-TIGER`)

start_er()
sleep(0.3)
stop_er()



#v = read_iq_signal("IQ_INPUT")
#y = readinternal("IQ_INPUT", N_samples)
#v[6000]
u = read_internal("IQ_AFTER_CTRL")



y_cav_input = read_iq_signal("DWN0")
y_phref = read_iq_signal("DWN1")
y = y_cav_input ./ (y_phref ./ abs.(y_phref))

#y = read_iq_signal("PIERRILC")


u = u[1:10000]
y = y[1:length(u)]

t = range(0, step=Ts_reg, length=length(u))

plot(t, real(u))



obj(p) = norm(ControlSystems.ltitr(fill(complex(p[1], p[2]),1,1), fill(complex(p[3], p[4]),1,1), u) - y) # Should re-write to use lsim
@time sol = optimize(obj, zeros(4))

println("Appox error: $(sol.minimum), opt sol: $(sol.minimizer)")
p = sol.minimizer

G = ss(complex(p[1], p[2]), complex(p[3], p[4]), 1, 0, Ts_reg)

a = log(complex(p[1], p[2]))/Ts_reg/(2π)
println("estimated bw: $(-real(a)/1e3) kHz, estimated detuning: $(imag(a)/1e3) kHz")


angle_error = angle(G.B[1,1]) # Not quite the angle error...
println("Angle offset: $angle_error")

# set_predist_mat(rot_mat(-angle_error))

##
figure(2)
clf()
#subplot(2, 1, 1)
plot(real(y));
plot(imag(y));
plot(0.01real(u));

#subplot(2, 1, 2)


show(block=false)

##


# A = [y u][1:end-1, :]
# A2 = [real(A) -imag(A); imag(A) real(A)]
# ydiff2 = [real(diff(y)); imag(diff(y))]
#
# theta2 = A2 \ ydiff2

#nr = length(u)-1
#plot([ydiff2[1:nr] A2[1:nr,:]*theta2])
#plot(imag([diff(y) A*theta]))

#G = ss(theta[1], theta[2], 1, 0, Ts_reg)
#y_hat = ControlSystems.ltitr(G.A, G.B, u)

#set_predist_mat(rot_mat(-(-3.1)))

y_hat = ControlSystems.ltitr(G.A, G.B, u)
norm(y - y_hat)


##
figure(3)
clf()
inds = 2000:3000
inds = 1:length(t)
subplot(2, 1, 1)
plot_iq(t[inds], 0.03u[inds]);
#plot_iq(t[inds], w[inds]);
plot_iq(t[inds], shiftsignal(y[inds], -6));

#p2 = plot();
subplot(2, 1, 2)
plot_iq(t[inds], y[inds]);


#p1 = plot(t[1:length(y_sim)], real(y_sim));
#p2 = plot(t[1:length(y_sim)], imag(y_sim));

#plot(p1, p2, layout=(2,1), link=:x, ticks=:native, size=(1200,800))
