using ControlSystems
using DSP
ENV["MPLBACKEND"]="qt5agg"
using PyPlot


include("help_fcns.jl")
include("IterativeLearningControl.jl")
include("PyControlPlots.jl")


# Set up cavity parameters
γ = 2π*80e3
τ = round(0.7e-6 / Ts_reg)*Ts_reg

P = c2d(DemoSystems.fotd(;T=1/γ, τ=τ), Ts_reg)

# Specify PI parameters
K = 1
Ki_mul_Ts = 10e-3

set_pi_params(K, Ki_mul_Ts)

# Model of the controller
C = ss(1, 1, Ki_mul_Ts, K, Ts_reg)


## tuning parameters for the ILC algorithm
κ = 0.5
delta = -20
Qbar = PolynomialRatio(digitalfilter(Lowpass(2.0e5, fs=1/h), Butterworth(2)))




##

N_iters = 50

h = G_yr.Ts
t = 0:h:3000e-6

T = ComplexF64

d_fake = -0.8 * T.(t .> 400e-6) # Simulate the beam-loading disturbance
write_ff_table(d_fake)

u_ILC = zeros(T, size(t))

e = zeros(T, length(t))

y_hist = zeros(T, length(t), N_iters)
u_hist = zeros(T, length(t), N_iters)




##


function run_iteration(u_ILC)
    run_pulses(0.5)

    #y = read_ctrl_input()[1:length(t)]
    y = -read_iq_signal("PIERRILC")[1:length(t)]    


    Cy = lsim(C, y[:])[1][:]    

    e .= -Cy[:]
    
    IterativeLearningControl.shift!(e, delta, e[end])

    u_ILC += κ * e


    u_ILC = filtfilt(Qbar, u_ILC)

    write_ff_table(u_ILC)
end

run_iteration(u_ILC)


for iter=1:1
    global u_ILC

    #y = lsim(G_yd, d + u_ILC)[1][:]

    run_pulses(0.5)

    #y = read_ctrl_input()[1:length(t)]
    y = -read_iq_signal("PIERRILC")[1:length(t)]    


    Cy = lsim(C, y[:])[1][:]    

    e .= -Cy[:]
    
    IterativeLearningControl.shift!(e, delta, e[end])

    global u_ILC += κ * e


    u_ILC = filtfilt(Qbar, u_ILC)

    write_ff_table(d_fake + u_ILC)

    global u_hist[:, iter] .= u_ILC
    global y_hist[:, iter] .= y[:]
end

u_hist, y_hist




#@time u_hist, y_hist = run_ILC(d)
#close()
figure(2)

#clf()
subplot(2, 1, 1, title="Cavity field")
plot_iq(t, y_hist)

subplot(2, 1, 2, title="u_ILC")
plot_iq(t, u_hist)

show(block=false)
#plot(t, r, "k:")




[norm(e_hist[:, k]) for k=1:size(e_hist, 2)]



# Transfer functions
Ω = 2π*logspace(2, 6.3, 200)

C_fr = freqresp(C, Ω)[:]
P_fr = freqresp(P, Ω)[:]
L_fr = freqresp(P*C, Ω)[:]


loglog(Ω/2π, abs.(P_fr))
loglog(Ω/2π, abs.(C_fr))
loglog(Ω/2π, abs.(L_fr))
show(block=false)

G_yd = feedback(P, C)
G_yr = feedback(P*C, 1)



# Investigate stability of ILC iterations
Qbar_tf = tf(Qbar.b, Qbar.a, Discrete(h))
Qbar_fr = freqresp(Qbar_tf, Ω)[:]

G_yr_fr = freqresp(G_yr, Ω)[:]
G_yd_fr = freqresp(G_yd, Ω)[:]
L_fr = κ * exp.(-im*h*delta * Ω)

A_fr = Qbar_fr .* conj(Qbar_fr) .* (1 .- L_fr .* C_fr .* G_yd_fr)

println(maximum(abs.(A_fr)))

#A_fr = 1 .- L_fr .* G_fr 
#A_fr = Qbar_fr .* conj(Qbar_fr)
figure(5)
PyControlPlots.bodeplot(Ω/2π, A_fr)
show(;block=false)
