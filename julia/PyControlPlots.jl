module PyControlPlots

using PyPlot
using ControlSystems

function bodeplot(Ω, G)

    
    if G isa Vector
        G_fr = G
    else
        G_fr = freqresp(G, Ω)[:]
    end

    subplot(2, 1, 1)
    loglog(Ω, abs.(G_fr))

    subplot(2, 1, 2)
    semilogx(Ω, rad2deg.(angle.(G_fr)))
end



end