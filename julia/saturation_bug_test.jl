N_DAQ_SAMPLES = 32*4000
N_samples = 30000

t = Ts* (0:N_DAQ_SAMPLES-1)
t_ms = 1000*t


t_reg = Ts_reg*(0:N_samples-1)

u0 = 0.8*squarewave(4e4, t)
u0 = [0; u0; 0] # Pad with zeros as fix for old bug, one zero should be enough

#set_pi_params(0.4, 1200e-4)
#set_pi_params(0.5, 0.1)
set_pi_params()

#1e-4
write_sp_table(zeros(N_DAQ_SAMPLES))
write_ff_table(u0)




#run_pulses(3)



N_samples = 6000

t_reg = Ts_reg * (0:N_samples-1)

@time y = readinternal("IQ_INPUT", N_samples)
pi_error = readinternal("PI_ERROR", N_samples)
u = readinternal("IQ_AFTER_CTRL", N_samples)
v = readinternal("VM_OUTPUT", N_samples)



plot1 = plot();
plot_iq(t_reg, y);

plot2 = plot();
plot_iq(t_reg, pi_error);

plot3 = plot();
plot_iq(t_reg, u);

plot4 = plot();
plot_iq(t_reg, v*exp(-im*2.74));

plot(plot1, plot2, plot3, plot4, layout=(4,1))
