"""
    `C, p = amigo_pid_design(T, L, Kp=1)`
    `C, p = amigo_pi_design(T, L, Kp=1)`

    Designs a PI(D) controller for a process approximation of the form

    `P(s) = Kp/(1 + Ts)*exp(-s*L)`

    where

    `T` - dominant lag time constant
    `L` - time delay
    `Kp` - process gain

    Returns the controller transfer function `C` and the parameters `p = [K, Ti, (Td)]`

    Note that an additional low-pass filter should be included in pratical PID designs.
"""
function amigo_pid_design(T, L, Kp=1)
    K = (0.2 + 0.45*T/L) / Kp
    Ti = (0.4*L + 0.8*T)/(L + 0.1*T)*L
    Td = 0.5*L*T / (0.3*L + T)

    #C = K * (1 + 1/Ti/s + Td*s)
    return [K, Ti, Td]
end

function amigo_pi_design(T, L, Kp=1)
    K = 0.15 / Kp + (0.35 - L*T/(L+T)^2)*T/Kp/L;
    Ti = 0.35*L + 13*L*T^2/(T^2 + 12*L*T + 7*L^2);

    #C = K * (1 + 1/Ti/s)
    return [K, Ti]
end

pi_ss(K, Ti, Tf) = ss([-1/Tf 0; 1 0], [1/Tf; 0], [K K/Ti], 0)

logspace(a, b, N::Integer) = exp10.(LinRange(a, b, N))