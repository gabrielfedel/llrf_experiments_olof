#include("init_llrf.jl")
#using PyPlot; pygui(:tk)

using Plots
pyplot()

N_DAQ_SAMPLES = 32*4000
N_samples = 30000

t = Ts* (0:N_DAQ_SAMPLES-1)
t_ms = 1000*t
w_IF = 2*pi*M_niq/N_niq/Ts

t_reg = Ts_reg*(0:N_samples-1)

#u0 = 0.5*squarewave(1.5e4, t)
u0 = [u0; 0] # Pad with zeros as fix for old bug, one zero should be enough

#set_pi_params(0.4, 1200e-4)
set_pi_params()

#1e-4
write_sp_table(zeros(N_DAQ_SAMPLES))
write_ff_table(u0)

llrf.set_fsm_state(IOC_NAME, "RESET")
llrf.set_fsm_state(IOC_NAME, "INIT")


llrf.set_fsm_state(IOC_NAME, "ON")
#llrf_board[:init_done]()
start_er()
sleep(0.2)
stop_er()
#force_pulse()

#ion()


#figure(1, block=false)#, show=false)
#clf()
#show(block=false)

y_cav = ddc_niq(caget("AI0"), 3, 14)
y_mo = ddc_niq(caget("AI1"), 3, 14)

y_vm = ddc_niq(caget("AI7"), 3, 14)



t = Ts*(0:length(y_cav)-1)

#plot()
#plot_iq(t, y_cav)


N_samples = 6000
y = readinternal("IQ_INPUT", N_samples)
pi_error = readinternal("PI_ERROR", N_samples)
u = readinternal("IQ_AFTER_CTRL", N_samples)
v = readinternal("VM_OUTPUT", N_samples)

t_reg = Ts_reg * (0:N_samples-1)

plot1 = plot();
plot_iq(t_reg, y);

plot2 = plot();
plot_iq(t_reg, pi_error);

plot3 = plot();
plot_iq(t_reg, u);

plot4 = plot();
#plot_iq(t_reg, v*exp(-im*2.74)); # the deroation seems to be a bi off
plot_iq(t, y_vm) # the deroation seems to be a bi off

plot(plot1, plot2, plot3, plot4, layout=(4,1))





pulse_rate = 14

div = round(Int, 88061948.02 / pulse_rate)
runmode = 0 # runmode normal
trgsrc = 1 # trigger source is software
res = 3 # ns

caput("TR-$IOC_NAME:PS0-Div-SP", div)
caput("TR-$IOC_NAME:SoftSeq0-RunMode-Sel", runmode)
caput("TR-$IOC_NAME:SoftSeq0-TrigSrc-2-Sel", trgsrc)
caput("TR-$IOC_NAME:SoftSeq0-TsResolution-Sel", res)

pulse_spacing = int((1.0/pulse_rate)*1000000000) # pulse size in ns

evtcode_vec = []
timestamp_vec = []

for k=1:N_pulses
    newts_vec = k*pulse_spacing)
    append!(evtcode_vec, [14, 15, 16, 17])
    append!(timestamp_vec, [newts, newts+10000, newts+310000, newts+(pulse_length*1000)+310000])
end

evtcode.append(127)
timestamp.append((timestamp[-1]) + 10000)

caput("TR-$IOC_NAME:SoftSeq0-EvtCode-SP", evtcode)
caput("TR-$IOC_NAME:SoftSeq0-Timestamp-SP", timestamp)

caput("TR-$IOC_NAME:SoftSeq0-Load-Cmd", 1)
caput("TR-$IOC_NAME:SoftSeq0-Enable-Cmd", 1)
caput("TR-$IOC_NAME:SoftSeq0-Commit-Cmd", 1)
