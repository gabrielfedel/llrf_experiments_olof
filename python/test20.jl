#include("init_llrf.jl")
#using PyPlot; pygui(:tk)

using Optim

using Plots
pyplot()

#ENV["PLOTS_USE_ATOM_PLOTPANE"] = "false"

N_DAQ_SAMPLES = 32*4000
N_samples = 30000

t = Ts* (0:N_DAQ_SAMPLES-1)
t_ms = 1000*t
w_IF = 2*pi*M_niq/N_niq/Ts

t_reg = Ts_reg*(0:N_samples-1)

u0 = 0.5*squarewave(1.5e4, t)
#u0 = [u0; 0.5] # Pad with zeros as fix for old bug, one zero should be enough

#set_pi_params(0.4, 1200e-4)
set_pi_params()

#1e-4
SP_I_TABLE = zeros(N_DAQ_SAMPLES)
SP_Q_TABLE = zeros(N_DAQ_SAMPLES)
FF_I_TABLE = u0
FF_Q_TABLE = zeros(length(u0))

llrf.set_fsm_state(IOC_NAME, "RESET")
llrf.set_fsm_state(IOC_NAME, "INIT")


caput("PI-I-FIXEDFFEN", 0)
caput("PI-Q-FIXEDFFEN", 0)

caget("PI-I-FIXEDFFEN")
caget("PI-Q-FIXEDFFEN")

caput("FF-PT0-I", FF_I_TABLE)
caput("FF-PT0-Q", FF_Q_TABLE)
caput("FF-PT0-WRTBL.PROC", 1)

llrf.set_fsm_state(IOC_NAME, "ON")
#llrf_board[:init_done]()
start_er()
sleep(0.2)
stop_er()
#force_pulse()

#ion()


#figure(1, block=false)#, show=false)
#clf()
#show(block=false)

y_cav = ddc_niq(caget("AI0"), 3, 14)
y_mo = ddc_niq(caget("AI1"), 3, 14)

t = Ts*(0:length(y_cav)-1)

#plot()
#plot_iq(t, y_cav)


N_samples = 6000
y = readinternal("IQ_INPUT", N_samples)
pi_error = readinternal("PI_ERROR", N_samples)
u = readinternal("IQ_AFTER_CTRL", N_samples)
v = readinternal("VM_OUTPUT", N_samples)

t_reg = Ts_reg * (0:N_samples-1)

plot1 = plot();
plot_iq(t_reg, y);

plot2 = plot();
plot_iq(t_reg, pi_error);

plot3 = plot();
plot_iq(t_reg, u);

plot4 = plot();
plot_iq(t_reg, v*exp(-im*2.74)); # the deroation seems to be a bi off

plot(plot1, plot2, plot3, plot4, layout=(4,1))


#= Some syst id experiments
obj(p) = norm(ControlSystems.ltitr(fill(complex(p[1], p[2]),1,1), fill(complex(p[3], p[4]),1,1), u) - y)
sol = optimize(obj, theta2)
println("Appox error: $(sol.minimum), opt sol: $(sol.minimizer)")
p = sol.minimizer
G2 = ss(complex(p[1], p[2]), complex(p[3], p[4]), 1, 0, Ts_reg)




A = [y u][1:end-1, :]
A2 = [real(A) -imag(A); imag(A) real(A)]
ydiff2 = [real(diff(y)); imag(diff(y))]

theta2 = A2 \ ydiff2




#nr = length(u)-1
#plot([ydiff2[1:nr] A2[1:nr,:]*theta2])
#plot(imag([diff(y) A*theta]))

G = ss(theta[1], theta[2], 1, 0, Ts_reg)

y_hat = ControlSystems.ltitr(G.A, G.B, u)
y_hat = ControlSystems.ltitr(G2.A, G2.B, u)
norm(y - y_hat)
angle(G2.B[1,1])

plot()
plot_iq(t_reg, y_hat)
plot_iq(t_reg, y)



=#

# lsim(ss(1,1,1,1,1), real(u), 0:length(u)-1)
# lsim på ett Int system
# diskret lsim ska inte behöva tidsargument
# Failar på is smooth för komplex data




#using Plots
#pyplot()


#
#
# iq_input_q, iq_input_i = llrf_board[:IQ_INPUT].read(N_samples)
# err_q, err_i = llrf_board["PI_ERROR].read(N_samples)
# u_q, u_i = llrf_board["IQ_AFTER_CTRL].read(N_samples)
# v_q, v_i = llrf_board["VM_OUTPUT].read(N_samples)
#
#
#
#
# iq_input_i,iq_input_q = store_daq_channels_mod.get_channel_data_iq(llrf_board, "LLRF_MEM_CTRL_6_PARAM", N_DAQ_SAMPLES)
# iq_input = iq_input_i + im*iq_input_q
#
# err_i,err_q = store_daq_channels_mod.get_channel_data_iq(llrf_board, "LLRF_MEM_CTRL_4_PARAM", N_DAQ_SAMPLES)
# err = err_i + im*err_q
#
# u_i,u_q = store_daq_channels_mod.get_channel_data_iq(llrf_board, "LLRF_MEM_CTRL_7_PARAM", N_DAQ_SAMPLES)
# v_i,v_q = store_daq_channels_mod.get_channel_data_iq(llrf_board, "LLRF_MEM_CTRL_8_PARAM", N_DAQ_SAMPLES)
#
#
#
# gamma = 2*pi*10000
# Dw = 2*pi*20000
#
#
#
# adc_offset = mean(err[3000:4000])
# err .-= adc_offset
#
#
# N_delay = 8 # Samples of delay
#
# u_clip = u[1:2000]
#
# err_clip = err[1:length(u_clip)]
# err_clip_shift = err[1+N_delay:length(u_clip)+N_delay]
#
#
# t_ms_clip = t_ms[1:length(u_clip)]
#
#
#
# X = [err_clip_shift u_clip]
# theta_est = X \ diff([err_clip_shift; 0])
#
# a = theta_est[1] / Ts
# b = theta_est[2]
# #gamma_est =
#
# g = b*exp.(a*t) / Ts
#
# #y_sim = cumsum(X * theta_est)
# y_sim = filt(g, complex(u_clip))*Ts
# y_sim = [zeros(N_delay); y_sim[1:end-N_delay]]
#
# println("Approx error: $(norm(y_sim - err_clip))")
#
#
# println("a: $(a/2/pi), b: $(b/abs(b))")
#
#
#
# p1 = plot(t_ms, real(err));
# plot!(t_ms_clip, real(y_sim));
# plot!(t_ms_clip, u_clip);
# #plot!(t_ms, real(g));
#
# p2 = plot(t_ms, imag(err));
# plot!(t_ms_clip, imag(y_sim));
#
#
# #p1 = plot(t[1:length(y_sim)], real(y_sim));
# #p2 = plot(t[1:length(y_sim)], imag(y_sim));
#
#
#
#
# plot(p1, p2, layout=(2,1), link=:x, ticks=:native, size=(1200,800))


#= QUESTION:

time for data transfer?!

=#
