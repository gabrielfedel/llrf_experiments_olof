from epics import caput, ca

rate = 14 # Hz
length = 2600 # us
prefix = "TR-LLRF"
npulses = 10 # should be <= 510, because of EVR table limit

"""
Prepare EVR for a specific setup, with a rate, pulse length and number
of pulses.
Note: When this function is called it changes EVR configuration, so for other
modes (like start/stop pulses) need to setup EVR with other parameters
"""
def prepareEvr(prefix, rate, length, npulses):
    div = int(88061948.02 / rate)
    runmode = 0 # runmode normal
    trgsrc = 1 # trigger source is software
    res = 3 # ns

    caput(prefix + ":PS0-Div-SP", div)
    caput(prefix + ":SoftSeq0-RunMode-Sel", runmode)
    caput(prefix + ":SoftSeq0-TrigSrc-2-Sel", trgsrc)
    caput(prefix + ":SoftSeq0-TsResolution-Sel", res)

    P = int((1.0/rate)*1000000000) # pulse size in ns

    evtcode = []
    timestamp = []

    for i in range(npulses):
        newts = (i*P)
        evtcode += [14, 15, 16, 17]
        timestamp += [newts, newts+10000, newts+310000, newts+(length*1000)+310000]

    evtcode.append(127)
    timestamp.append((timestamp[-1]) + 10000)

    caput(prefix + ":SoftSeq0-EvtCode-SP", evtcode)
    caput(prefix + ":SoftSeq0-Timestamp-SP", timestamp)

    caput(prefix + ":SoftSeq0-Load-Cmd", 1)
    caput(prefix + ":SoftSeq0-Enable-Cmd", 1)
    caput(prefix + ":SoftSeq0-Commit-Cmd", 1)

    ca.poll(0.01) # time to allow the PVs be processed by the IOC

"""
When this function is called, after prepareEVR, it will run the
number of pulses configured before.
"""
def startPulses(prefix):
    caput(prefix + ":SoftSeq0-SoftTrig-Cmd", 1)

if __name__ == '__main__':
    prepareEvr(prefix, rate, length, npulses)
    startPulses(prefix)
