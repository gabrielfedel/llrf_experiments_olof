import epics
import matplotlib.pyplot as mplot
import numpy as np
import scipy.fftpack

import llrf
import lowlevhw



llrf_ioc.caput('VM-DC-OFFSET-I', dc_offset_i)
llrf_ioc.caput('VM-DC-OFFSET-Q', dc_offset_q)

time.sleep(0.1)
print("Running with DC offset correction: I={}, Q={}".format(dc_offset_i, dc_offset_q))

llrf_board.force_timing_pulse(5, 5)
time.sleep(.10)
acquired_data = llrf_board['DAQ_CH{}'.format(input_channel)].read()



f_sampling = llrf_ioc.caget('F-SAMPLING')
M = llrf_ioc.caget('IQSMPL-NEARIQM-RBV')
N = llrf_ioc.caget('IQSMPL-NEARIQN-RBV')
T = 1/(f_sampling*10**6)