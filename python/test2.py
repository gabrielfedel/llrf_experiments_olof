import import_and_setup_for_llrf



llrf.set_fsm_state("RESET")
llrf.set_fsm_state("INIT")


# Angle offset compensation (system dependent)
epics.caput(IOC_NAME + ":IQSMPL-ANGOFFSETVAL", 0)
epics.caput(IOC_NAME + ":IQSMPL-ANGOFFSETEN", 0)

epics.caput(IOC_NAME + ":AI-SMNM", N_DAQ_SAMPLES)

epics.caput(IOC_NAME + ":PI-I-GAINK", K_I)
epics.caput(IOC_NAME + ":PI-I-GAINTSDIVTI", TS_DIV_TI_I)
epics.caput(IOC_NAME + ":PI-Q-GAINK", K_Q)
epics.caput(IOC_NAME + ":PI-Q-GAINTSDIVTI", TS_DIV_TI_Q)

epics.caput(IOC_NAME + ":SP-PT0-I", SP_I_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-Q", SP_Q_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-WRTBL.PROC", 1)

epics.caput(IOC_NAME + ":FF-PT0-I", FF_I_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-Q", FF_Q_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-WRTBL.PROC", 1)

epics.caput(IOC_NAME + ":PI-I-FIXEDSPEN", 0)
epics.caput(IOC_NAME + ":PI-I-FIXEDFFEN", 0)
epics.caput(IOC_NAME + ":PI-Q-FIXEDSPEN", 0)
epics.caput(IOC_NAME + ":PI-Q-FIXEDSPEN", 0)


# Chenge loop phase by setting the predistortion to a rotation matrix
epics.caput(IOC_NAME + ":VM-PREDISTEN", 1)

set_rotation_mat(0.8, 0, 0, 0.8)




# new sample every clock cycle
llrf_board["LLRF_PI_1_CTRL"]["FF_TBL_SPEED"].write(0x0)

llrf.set_fsm_state("ON")


#plt.figure(5)
#plt.plot(epics.caget(IOC_NAME + ":FF-PT0-I"))
#plt.plot(FF_I_TABLE)


for i in range(4):
    (ramp_up_time, pulse_time) = llrf_board.force_timing_pulse(1, 2)
    time.sleep(1/14)

# Set circular bit for FF table
# llrf_board["LLRF_SPFF_CTRL_1_PARAM"]["FF_CIRCULAR_EN"].set()
# llrf_board["LLRF_SPFF_CTRL_2_PARAM"]["SP_CIRCULAR_EN"].set()



# %%

# Get raw data channels and manually convert to baseband
y_cav_raw = llrf_board["DAQ_CH0"].read(N_DAQ_SAMPLES//32)

len(y_cav_raw)
y_mo_raw = llrf_board["DAQ_CH1"].read(N_DAQ_SAMPLES//32)
y_vm_raw = llrf_board["DAQ_CH7"].read(N_DAQ_SAMPLES//32)

y_mo = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_mo_raw * np.exp(1j*w_IF*t))
y_cav = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_cav_raw * np.exp(1j*w_IF*t))
y_vm = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_vm_raw * np.exp(1j*w_IF*t))

# plt.figure(9)
# plt.subplot(3, 1, 1)
# plt.plot(t, np.real(y_cav))
# plt.plot(t, np.imag(y_cav))
#
# plt.subplot(3, 1, 2)
# plt.plot(t, np.real(y_mo))
# plt.plot(t, np.imag(y_mo))
#
# plt.subplot(3, 1, 3)
# plt.plot(t, np.real(y_vm))
# plt.plot(t, np.imag(y_vm))




# Find IF frequency..
# sp = np.fft.fft(y_mo_raw)
# req = np.fft.fftfreq(len(y_mo_raw), Ts)
#
# plt.figure(100)
# plt.plot(freq, abs(sp))


y_vm_rel = y_vm / y_mo

plt.figure(23)
plt.clf
plt.subplot(2, 1, 1)
plt.plot(1e3*t, np.real(y_vm_rel))
plt.plot(1e3*t, np.imag(y_vm_rel))

plt.show()


# %%

# Plot IQ channels
N_samples = 20000

# Note reversed order!! Perhaps better to return complex signals

iq_input_q, iq_input_i = llrf_board["IQ_INPUT"].read(N_samples)
err_q, err_i = llrf_board["PI_ERROR"].read(N_samples)
u_q, u_i = llrf_board["IQ_AFTER_CTRL"].read(N_samples)
v_q, v_i = llrf_board["VM_OUTPUT"].read(N_samples)


# 4 PI errror
# 5 after filters
# 6 after filters

# 7 after pi controllerII
# 8 at at the output


t_reg = Ts_reg*np.arange(N_samples)

plt.figure(11)
plt.subplot(4, 1, 1)
plt.plot(1e3*t_reg, iq_input_i)
plt.plot(1e3*t_reg, iq_input_q)
plt.scatter(1e3*t_reg, iq_input_q)
#plt.plot(t, -err_i)
#plt.plot(t, err_q)

plt.subplot(4, 1, 2)
plt.plot(1e3*t_reg, err_i)
plt.plot(1e3*t_reg, err_q)
plt.title("control error")


plt.subplot(4, 1, 3)
plt.plot(1e3*t_reg, u_i)
plt.plot(1e3*t_reg, u_q)
plt.title("After controller")

plt.subplot(4, 1, 4)
plt.plot(1e3*t_reg, v_i)
plt.plot(1e3*t_reg, v_q)
plt.title("VM output")

plt.xlabel("t [ms]")
plt.show()


# %%
plt.figure(25)
plt.clf
plt.subplot(2, 1, 1)
plt.plot(1e3*t, np.real(y_vm_rel))
plt.plot(1e3*t, np.imag(y_vm_rel))

plt.plot(1e3*t_reg, u_i)
plt.plot(1e3*t_reg, u_q)
plt.plot(1e3*t_reg, v_i)
plt.plot(1e3*t_reg, v_q)


plt.show()
