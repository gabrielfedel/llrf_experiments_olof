#!/usr/bin/env python3

#-------------------------------------------------------------------------------
#  Project     : ESS FPGA Framework
#-------------------------------------------------------------------------------
#  File        : store_daq_channels.py
#  Authors     : Christian Amstutz
#  Created     : 2018-10-22
#  Last update : 2019-01-09
#  Platform    : SIS8300-KU
#  Standard    :
#-------------------------------------------------------------------------------
#  Description :
#  Problems    :
#-------------------------------------------------------------------------------
#  Copyright (c) 2019 European Spallation Source ERIC
#-------------------------------------------------------------------------------
#  Revisions  :
#
#  0.1 : 2019-01-09  Christian Amstutz
#        Created
#-------------------------------------------------------------------------------

import os
import sys
import inspect
import csv
import matplotlib.pyplot as mplot
import numpy as np

# Load modules in other folders

helper_path = "/home/iocuser/llrf_v2_digital/scripts/helpers/"
import_folder = helper_path + "../../sw/python/llrf"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import llrf

import_folder = helper_path + "../../lib/hw/ess_fpga_framework/sw/python/essffw"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw


def iq_from_int(iq_sample, bits=32):
    part_bits = bits // 2
    mask = (2 ** part_bits) - 1
    i = ((iq_sample & (mask << part_bits)) >> part_bits)
    q = iq_sample & mask
    return (i, q)


def binoffset_to_float(value, bits):
    value /= 2**(bits-1)
    value -= 1
    return value


def twocompl_to_float(value, bits):
    value /= 2**(bits-1)
    if value >= 1:
        value -= 2
    return value


def get_channel_data(llrf_board, channel):
    samples_nr = (llrf_board["ADC_SAMPLE_LEN"].read()+1)*32

    ch_address_reg = "ADC_CH{}_MEM".format(channel+1)
    ch_address = llrf_board[ch_address_reg].read()
    ch_address = ((ch_address * 32)-samples_nr) * 2

    channel_memory = llrf_board[(ch_address, samples_nr)]
    channel_memory.sample_size = 2
    channel_data = channel_memory.read()
    channel_data_signed = np.array([twocompl_to_float(sample, 16) for sample in channel_data])

    return channel_data_signed
    #print("Channel {} ({} from 0x{:X}):".format(channel, samples_nr, ch_address))
    #print(channel_data_signed[0:100])
    #print()

def get_channel_data_iq(llrf_board, ch_address_reg, SAMPLES_NR):
    ch_address = llrf_board[ch_address_reg].read()
    channel_memory = llrf_board[(ch_address, SAMPLES_NR)]
    channel_memory.sample_size = 4

    channel_data = channel_memory.read()
    channel_data_iq = [iq_from_int(sample) for sample in channel_data]
    channel_data_i = np.array([twocompl_to_float(sample[0], 16) for sample in channel_data_iq])
    channel_data_q = np.array([twocompl_to_float(sample[1], 16) for sample in channel_data_iq])
    #print("Channel {} ({} from 0x{:X}):".format(channel, SAMPLES_NR, ch_address))
    #print([(channel_data_i[i], channel_data_q[i]) for i in range(100)])
    #print()
    return channel_data_i,channel_data_q

def store_daq_channels(llrf_board):
    DATA_DIR = "../../output/test"
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

    samples_nr = (llrf_board["ADC_SAMPLE_LEN"].read()+1)*32
    channels = range(10)
    for channel in channels:
        channel_data_signed = get_channel_data(llrf_board, channel)

        file_name = DATA_DIR + "/" + "ch{}_data.csv".format(channel+1)
        with open(file_name, 'w+', newline='') as csvfile:
            ch_writer = csv.writer(csvfile)
            ch_writer.writerow(channel_data_signed)

        if "DISPLAY" in os.environ:
            x = np.linspace(0, len(channel_data_signed)-1, len(channel_data_signed))
            mplot.figure(1)
            mplot.subplot(5, 2, channel+1)
            mplot.plot(x, channel_data_signed)
            mplot.title("Channel {}".format(channel))

    SAMPLES_NR = 5000
    channels = [("LLRF_MEM_CTRL_5_PARAM", "iq_input"),
                ("LLRF_MEM_CTRL_6_PARAM", "iq_after_filter"),
                ("LLRF_MEM_CTRL_4_PARAM", "pi_error"),
                ("LLRF_MEM_CTRL_7_PARAM", "iq_after_controller"),
                ("LLRF_MEM_CTRL_8_PARAM", "output")]

    i = 0
    for channel in channels:
        channel_data_i, channel_data_q = get_channel_data_iq(llrf_board, channel[0], SAMPLES_NR)

        file_name = DATA_DIR + "/" + "{}_data_i.csv".format(channel[1])
        with open(file_name, 'w+', newline='') as csvfile:
            ch_writer = csv.writer(csvfile)
            ch_writer.writerow(channel_data_i)
        file_name = DATA_DIR + "/" + "{}_data_q.csv".format(channel[1])
        with open(file_name, 'w+', newline='') as csvfile:
            ch_writer = csv.writer(csvfile)
            ch_writer.writerow(channel_data_q)

        if "DISPLAY" in os.environ:
            x = np.linspace(0, len(channel_data_i)-1, len(channel_data_i))
            mplot.figure(2)
            mplot.subplot(5, 2, 2*i+1)
            mplot.plot(x, channel_data_i)
            mplot.title("{}.i".format(channel[1]))
            mplot.subplot(5, 2, 2*i+2)
            mplot.plot(x, channel_data_q)
            mplot.title("{}.q".format(channel[1]))

        i += 1

    if "DISPLAY" in os.environ:
        mplot.show()

    print("Channel data written to folder {}".format(DATA_DIR))


if __name__ == "__main__":
    llrf_board = llrf.LLRFDevice()
    ku_boards = essffw.find_essffw_devices()
    llrf_board.connect(ku_boards[0])

    store_daq_channels(llrf_board)

    llrf_board.disconnect()
