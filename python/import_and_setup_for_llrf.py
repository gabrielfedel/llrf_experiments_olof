import os
import sys
import inspect
import csv
import time
import matplotlib.pyplot as plt
import numpy as np

from numpy import cos, sin, zeros, pi


import scipy.signal as signal

import epics

IOC_NAME = "LLRF-TIGER1"


# Must be currently a value that is a multiple of 32
N_DAQ_SAMPLES = 32*8000

N_niq = 14
w_IF = 2*pi*25.07e6;



fs = epics.caget(IOC_NAME + ":F-SAMPLING")

fs = 117.403333333e6
#w_IF = 2*pi*352.21/3/14*3; fs = 352.21e6 / 3







Ts = 1 / fs
Ts_reg = N_niq*Ts

t = Ts*np.arange(N_DAQ_SAMPLES)

K_I = 0
TS_DIV_TI_I = 0#5e-5
K_Q = 0
TS_DIV_TI_Q = 0#1e-4
SP_I_TABLE = [0.0] * 4000
SP_Q_TABLE = [0.0] * 4000
#FF_I_TABLE = np.concatenate((0.5*np.ones(10), [0.8] * signal.square(2*pi*5e3 * Ts*np.arange(5*N_DAQ_SAMPLES)), zeros(1000)))

FF_I_TABLE = np.concatenate(([0], 0.8 * signal.square(2*pi*20e3 * Ts*np.arange(N_DAQ_SAMPLES)), zeros(1000), [0]))
FF_Q_TABLE = zeros(5000)
################################################################################

helper_path = "/home/iocuser/llrf_v2_digital/scripts/helpers/"
import_folder = helper_path + "../../sw/python/llrf"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import llrf

import_folder = helper_path + "../../lib/hw/ess_fpga_framework/sw/python/essffw"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw


def set_rotation_mat(R11, R12, R21, R22):
    epics.caput(IOC_NAME + ":VM-PREDIST-RC00", R11)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC01", R12)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC10", R21)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC11", R22)

def set_rotation(theta_adj):
    set_rotation_mat(cos(theta_adj), -sin(theta_adj), sin(theta_adj), cos(theta_adj))


llrf_board = llrf.LLRFDevice()
ku_boards = essffw.find_essffw_devices()
llrf_board.connect(ku_boards[0])
