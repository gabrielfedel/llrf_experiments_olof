# Script parameters ############################################################

import os
import sys
import inspect
import csv
import time
import matplotlib.pyplot as plt
import numpy as np

from numpy import cos, sin, zeros, pi


import scipy.signal as signal

import epics

IOC_NAME = "LLRF-TIGER"

def stop_er(ioc):
    epics.caput("TR-" + ioc + ":SoftSeq0-Disable-Cmd", 1)

def start_er(ioc):
    epics.caput("TR-" + ioc + ":SoftSeq0-Enable-Cmd", 1)


plt.ioff()



# Must be currently a value that is a multiple of 32




N_niq = 14
w_IF = 2*pi*25.07e6;



fs = epics.caget(IOC_NAME + ":F-SAMPLING")

fs

fs = 117.403333333e6
#w_IF = 2*pi*352.21/3/14*3; fs = 352.21e6 / 3



Ts = 1 / fs
Ts_reg = N_niq*Ts


N_samples = round(0.6e-3 / Ts_reg)
N_DAQ_SAMPLES = round(0.6e-3 / Ts / 32) * 32

t = Ts*np.arange(N_DAQ_SAMPLES)


K_I = 0
TS_DIV_TI_I = 0#5e-5
K_Q = 0
TS_DIV_TI_Q = 0#1e-4
SP_I_TABLE = [0.0] * 4000
SP_Q_TABLE = [0.0] * 4000
#FF_I_TABLE = np.concatenate((0.5*np.ones(10), [0.8] * signal.square(2*pi*5e3 * Ts*np.arange(5*N_DAQ_SAMPLES)), zeros(1000)))

FF_I_TABLE = np.concatenate(([0], 0.8 * signal.square(2*pi*20e3 * Ts*np.arange(N_DAQ_SAMPLES)), zeros(1000), [0]))
FF_Q_TABLE = FF_I_TABLE #zeros(5000)
################################################################################

helper_path = "/home/iocuser/llrf_v2_digital/scripts/helpers/"
import_folder = helper_path + "../../sw/python/llrf"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import llrf

import_folder = helper_path + "../../lib/hw/ess_fpga_framework/sw/python/essffw"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw


def set_rotation_mat(R11, R12, R21, R22):
    epics.caput(IOC_NAME + ":VM-PREDIST-RC00", R11)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC01", R12)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC10", R21)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC11", R22)

def set_rotation(theta_adj):
    set_rotation_mat(cos(theta_adj), -sin(theta_adj), sin(theta_adj), cos(theta_adj))


llrf_board = llrf.LLRFDevice()
ku_boards = essffw.find_essffw_devices()
llrf_board.connect(ku_boards[0])



#llrf.set_fsm_state(IOC_NAME, "RESET")
#llrf.set_fsm_state(IOC_NAME, "INIT")


# Angle offset compensation (system dependent)
epics.caput(IOC_NAME + ":IQSMPL-ANGOFFSETVAL", 0)
epics.caput(IOC_NAME + ":IQSMPL-ANGOFFSETEN", 0)

epics.caput(IOC_NAME + ":AI-SMNM", N_DAQ_SAMPLES)

epics.caput(IOC_NAME + ":PI-I-GAINK", K_I)
epics.caput(IOC_NAME + ":PI-I-GAINTSDIVTI", TS_DIV_TI_I)
epics.caput(IOC_NAME + ":PI-Q-GAINK", K_Q)
epics.caput(IOC_NAME + ":PI-Q-GAINTSDIVTI", TS_DIV_TI_Q)

epics.caput(IOC_NAME + ":SP-PT0-I", SP_I_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-Q", SP_Q_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-WRTBL.PROC", 1)

epics.caput(IOC_NAME + ":FF-PT0-I", FF_I_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-Q", FF_Q_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-WRTBL.PROC", 1)

epics.caput(IOC_NAME + ":PI-I-FIXEDSPEN", 0) #does not seems to return
epics.caput(IOC_NAME + ":PI-I-FIXEDFFEN", 0)
epics.caput(IOC_NAME + ":PI-Q-FIXEDSPEN", 0)
epics.caput(IOC_NAME + ":PI-Q-FIXEDSPEN", 0)

#epics.caput("LLRF-TIGER:PI-I-FIXEDSPEN", 0)
# Chenge loop phase by setting the predistortion to a rotation matrix
epics.caput(IOC_NAME + ":VM-PREDISTEN", 1)



#set_rotation_mat(0.8, 0, 0, 0.8)

set_rotation(0.0)




# new sample every clock cycle
llrf_board["LLRF_PI_1_CTRL"]["FF_TBL_SPEED"].write(0x0)


#llrf.set_fsm_state(IOC_NAME, "ON")


epics.caput(IOC_NAME + ":SP-PT0-I", SP_I_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-Q", SP_Q_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-WRTBL.PROC", 1)

epics.caput(IOC_NAME + ":FF-PT0-I", FF_I_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-Q", FF_Q_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-WRTBL.PROC", 1)

#plt.figure(5)
#plt.plot(epics.caget(IOC_NAME + ":FF-PT0-I"))
#plt.plot(FF_I_TABLE)


for i in range(4):
    (ramp_up_time, pulse_time) = llrf_board.force_timing_pulse(1, 2)
    time.sleep(1/14)

# Set circular bit for FF table
# llrf_board["LLRF_SPFF_CTRL_1_PARAM"]["FF_CIRCULAR_EN"].set()
# llrf_board["LLRF_SPFF_CTRL_2_PARAM"]["SP_CIRCULAR_EN"].set()
#plt.close('all')

start_er(IOC_NAME)
#stop_er(IOC_NAME)


# %%

t = Ts*np.arange(N_DAQ_SAMPLES)

y_cav_raw = llrf_board.__getitem__("DAQ_CH0").read(N_DAQ_SAMPLES//32)


# Get raw data channels and manually convert to baseband
y_cav_raw = llrf_board["DAQ_CH0"].read(N_DAQ_SAMPLES//32)

y_mo_raw = llrf_board["DAQ_CH1"].read(N_DAQ_SAMPLES//32)

y_refl_raw = llrf_board["DAQ_CH2"].read(N_DAQ_SAMPLES//32)

y_vm_raw = llrf_board["DAQ_CH7"].read(N_DAQ_SAMPLES//32)

y_mo = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_mo_raw * np.exp(1j*w_IF*t))

y_cav = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_cav_raw * np.exp(1j*w_IF*t)) / y_mo
y_vm = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_vm_raw * np.exp(1j*w_IF*t)) / y_mo


y_refl = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_refl_raw * np.exp(1j*w_IF*t)) / y_mo


plt.figure(9);
plt.subplot(3, 1, 1);
plt.plot(t, np.real(y_cav))
plt.plot(t, np.imag(y_cav))

#plt.subplot(3, 1, 2)
#plt.plot(t, np.real(y_mo))
#plt.plot(t, np.imag(y_mo))

plt.subplot(3, 1, 3)
plt.plot(t, np.real(y_vm))
plt.plot(t, np.imag(y_vm))

plt.show()


# Find IF frequency..
# sp = np.fft.fft(y_mo_raw)
# req = np.fft.fftfreq(len(y_mo_raw), Ts)
#
# plt.figure(100)
# plt.plot(freq, abs(sp))

#plt.figure(10)
#plt.plot(FF_I_TABLE)


# %%

# Plot IQ channels


# Note reversed order!! Perhaps better to return complex signals

iq_input_q, iq_input_i = llrf_board["IQ_INPUT"].read(N_samples)
err_q, err_i = llrf_board["PI_ERROR"].read(N_samples)
u_q, u_i = llrf_board["IQ_AFTER_CTRL"].read(N_samples)
v_q, v_i = llrf_board["VM_OUTPUT"].read(N_samples)

# 4 PI errror
# 5 after filters
# 6 after filters

# 7 after pi controllerII
# 8 at at the output


t_reg = Ts_reg*np.arange(N_samples)


plt.figure(11);
plt.subplot(4, 1, 1)
plt.plot(1e3*t_reg, iq_input_i)
plt.plot(1e3*t_reg, iq_input_q)
plt.scatter(1e3*t_reg, iq_input_q)
#plt.plot(t, -err_i)
#plt.plot(t, err_q)

plt.subplot(4, 1, 2)
plt.plot(1e3*t_reg, err_i)
plt.plot(1e3*t_reg, err_q)
plt.title("control error")


plt.subplot(4, 1, 3)
plt.plot(1e3*t_reg, u_i)
plt.plot(1e3*t_reg, u_q)
plt.title("After controller")

plt.subplot(4, 1, 4)
plt.plot(1e3*t_reg, v_i)
plt.plot(1e3*t_reg, v_q)
plt.title("VM output")

plt.xlabel("t [ms]")
plt.show()


# %%
plt.figure(25);
plt.clf
plt.subplot(2, 1, 1)
plt.plot(1e3*t, np.real(y_vm_rel))
plt.plot(1e3*t, np.imag(y_vm_rel))

plt.plot(1e3*t_reg, u_i)
plt.plot(1e3*t_reg, u_q)
plt.plot(1e3*t_reg, v_i)
plt.plot(1e3*t_reg, v_q)


plt.show()

#llrf_board.disconnect()
