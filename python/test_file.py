# Script parameters ############################################################

import os
import sys
import inspect
import csv
import time
import matplotlib.pyplot as plt
import numpy as np

from numpy import cos, sin, zeros, pi

# run the setup script!!!
# ~/llrf_v2_digital/scripts/helpers/llrf_test_setup.py -f 117
#  Also need to turn ON in teh screen or through command..

import scipy.signal as signal

import epics

IOC_NAME = "LLRF-TIGER"

def stop_er(ioc):
    epics.caput("TR-" + ioc + ":SoftSeq0-Disable-Cmd", 1)

def start_er(ioc):
    epics.caput("TR-" + ioc + ":SoftSeq0-Enable-Cmd", 1)


plt.ioff()

# Must be currently a value that is a multiple of 32

N_niq = 14
w_IF = 2*pi*25.07e6;


fs = epics.caget(IOC_NAME + ":F-SAMPLING")

fs

fs = 117.403333333e6
#w_IF = 2*pi*352.21/3/14*3; fs = 352.21e6 / 3



Ts = 1 / fs
Ts_reg = N_niq*Ts


N_samples = round(0.6e-3 / Ts_reg)
N_DAQ_SAMPLES = round(0.6e-3 / Ts / 32) * 32

t = Ts*np.arange(N_DAQ_SAMPLES)

K = 0
Ti = 0#1e-5
K_I = K
TS_DIV_TI_I = Ti
K_Q = K
TS_DIV_TI_Q = Ti
SP_I_TABLE = [0.0] * 4000
SP_Q_TABLE = [0.0] * 4000
#FF_I_TABLE = np.concatenate((0.5*np.ones(10), [0.8] * signal.square(2*pi*5e3 * Ts*np.arange(5*N_DAQ_SAMPLES)), zeros(1000)))

FF_I_TABLE = np.concatenate(([0], 0.8 * signal.square(2*pi*20e3 * Ts*np.arange(N_DAQ_SAMPLES)), zeros(1000), [0]))
FF_Q_TABLE = FF_I_TABLE #zeros(5000)
################################################################################

helper_path = "/home/iocuser/llrf_v2_digital/scripts/helpers/"
import_folder = helper_path + "../../sw/python/llrf"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import llrf

import_folder = helper_path + "../../lib/hw/ess_fpga_framework/sw/python/essffw"
import_folder_abs = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe() ))[0], import_folder)))
if import_folder_abs not in sys.path:
    sys.path.insert(0, import_folder_abs)
import essffw


def set_rotation_mat(R11, R12, R21, R22):
    epics.caput(IOC_NAME + ":VM-PREDIST-RC00", R11)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC01", R12)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC10", R21)
    epics.caput(IOC_NAME + ":VM-PREDIST-RC11", R22)

def set_rotation(theta_adj):
    set_rotation_mat(cos(theta_adj), -sin(theta_adj), sin(theta_adj), cos(theta_adj))




def read_internal_iq(sig_name, N_samples):
    q_vec, i_vec = llrf_board[sig_name].read(N_samples)
    return np.array([complex(i_vec[k], q_vec[k]) for k in range(0,len(q_vec))])

def plot_iq(t, y):
    plt.plot(t, np.real(y))
    plt.plot(t, np.imag(y))


llrf_board = llrf.LLRFDevice()
ku_boards = essffw.find_essffw_devices()
llrf_board.connect(ku_boards[0])



llrf.set_fsm_state(IOC_NAME, "RESET")
llrf.set_fsm_state(IOC_NAME, "INIT")


# Angle offset compensation (system dependent)
epics.caput(IOC_NAME + ":IQSMPL-ANGOFFSETVAL", 0)
epics.caput(IOC_NAME + ":IQSMPL-ANGOFFSETEN", 0)

epics.caput(IOC_NAME + ":AI-SMNM", N_DAQ_SAMPLES)

epics.caput(IOC_NAME + ":PI-I-GAINK", K_I)
epics.caput(IOC_NAME + ":PI-I-GAINTSDIVTI", TS_DIV_TI_I)
epics.caput(IOC_NAME + ":PI-Q-GAINK", K_Q)
epics.caput(IOC_NAME + ":PI-Q-GAINTSDIVTI", TS_DIV_TI_Q)

epics.caput(IOC_NAME + ":SP-PT0-I", SP_I_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-Q", SP_Q_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-WRTBL.PROC", 1)

FF_I_TABLE


epics.caput(IOC_NAME + ":FF-PT0-I", FF_I_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-Q", FF_Q_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-WRTBL.PROC", 1)

# epics.caput(IOC_NAME + ":PI-I-FIXEDSPEN", 0) does not seems to return
epics.caput(IOC_NAME + ":PI-I-FIXEDFFEN", 0)
epics.caput(IOC_NAME + ":PI-Q-FIXEDSPEN", 0)
epics.caput(IOC_NAME + ":PI-Q-FIXEDSPEN", 0)

#epics.caput("LLRF-TIGER:PI-I-FIXEDSPEN", 0)
# Chenge loop phase by setting the predistortion to a rotation matrix
epics.caput(IOC_NAME + ":VM-PREDISTEN", 1)



#set_rotation_mat(0.8, 0, 0, 0.8)

set_rotation(2.74)




# new sample every clock cycle
llrf_board["LLRF_PI_1_CTRL"]["FF_TBL_SPEED"].write(0x0)


llrf.set_fsm_state(IOC_NAME, "ON")


epics.caput(IOC_NAME + ":SP-PT0-I", SP_I_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-Q", SP_Q_TABLE)
epics.caput(IOC_NAME + ":SP-PT0-WRTBL.PROC", 1)

epics.caput(IOC_NAME + ":FF-PT0-I", FF_I_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-Q", FF_Q_TABLE)
epics.caput(IOC_NAME + ":FF-PT0-WRTBL.PROC", 1)

#plt.figure(5)
#plt.plot(epics.caget(IOC_NAME + ":FF-PT0-I"))
#plt.plot(FF_I_TABLE)


for i in range(4):
    (ramp_up_time, pulse_time) = llrf_board.force_timing_pulse(1, 2)
    time.sleep(1/14)

# Set circular bit for FF table
# llrf_board["LLRF_SPFF_CTRL_1_PARAM"]["FF_CIRCULAR_EN"].set()
# llrf_board["LLRF_SPFF_CTRL_2_PARAM"]["SP_CIRCULAR_EN"].set()
#plt.close('all')

start_er(IOC_NAME)
#stop_er(IOC_NAME)


# %%

t = Ts*np.arange(N_DAQ_SAMPLES)

#y_cav_raw = llrf_board.__getitem__("DAQ_CH0").read(N_DAQ_SAMPLES//32)


y = epics.caget(IOC_NAME + ":AI0")

# Get raw data channels and manually convert to baseband
y_cav_raw = epics.caget(IOC_NAME + ":AI0")
y_mo_raw = epics.caget(IOC_NAME + ":AI1")
#y_refl_raw = epics.caget(IOC_NAME + ":AI2")
y_vm_raw = epics.caget(IOC_NAME + ":AI7")

y_mo = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_mo_raw * np.exp(1j*w_IF*t))
y_cav = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_cav_raw * np.exp(1j*w_IF*t)) / y_mo
y_vm = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_vm_raw * np.exp(1j*w_IF*t)) / y_mo

#y_refl = signal.lfilter(np.ones(N_niq)/N_niq, 1, y_refl_raw * np.exp(1j*w_IF*t)) / y_mo


plt.figure(9);
plt.clf()
plt.subplot(2, 1, 1);
plt.plot(t, np.real(y_cav))
plt.plot(t, np.imag(y_cav))

plt.subplot(2, 1, 2)
plt.plot(t, np.real(y_vm))
plt.plot(t, np.imag(y_vm))

plt.show()


# Find IF frequency..
# sp = np.fft.fft(y_mo_raw)
# req = np.fft.fftfreq(len(y_mo_raw), Ts)
#
# plt.figure(100)
# plt.plot(freq, abs(sp))

#plt.figure(10)
#plt.plot(FF_I_TABLE)


# Plot IQ channels


# Note reversed order!! Perhaps better to return complex signals



#np.array(np.diff(y),




y = read_internal_iq("IQ_INPUT", N_samples)
err = read_internal_iq("PI_ERROR", N_samples)
u = read_internal_iq("IQ_AFTER_CTRL", N_samples)
v = read_internal_iq("VM_OUTPUT", N_samples)

t = time.process_time()
llrf_board["VM_OUTPUT"].read(5000);
elapsed_time = time.process_time() - t
print(elapsed_time)


len(np.diff(y))
A = np.transpose(np.vstack((y[0:-1], u[0:-1])));
b = np.diff(y) #/ Ts_reg
m = np.linalg.lstsq(A, b, rcond=None)

gamma_est = m[0][0]
k_est = m[0][0]

gamma_est

y_hat = np.zeros(len(u), dtype=complex)

for k in np.arange(len(u)-1):
    y_hat[k+1] = y_hat[k] + (gamma_est*y_hat[k] + k_est*u[k])




t_reg = Ts_reg*np.arange(N_samples)


#y_hat.shape[1]
plt.figure(3);
plt.clf()
plot_iq(t_reg, y_hat)
plot_iq(t_reg, 2*y)
plt.show(block=False)

u[0:10]
print(A[:,0:50])


plt.figure(9); plt.clf()
plt.plot(np.imag(u))
plt.show(block=False)

# 4 PI errror
# 5 after filters
# 6 after filters

# 7 after pi controllerII
# 8 at at the output



plt.figure(11);
plt.subplot(4, 1, 1)
plot_iq(1e3*t_reg, y)
plt.title("y (iq input)")

plt.subplot(4, 1, 2)
plot_iq(1e3*t_reg, err)
plt.title("control error")


plt.subplot(4, 1, 3)
plot_iq(1e3*t_reg, u)
plt.title("After controller")

plt.subplot(4, 1, 4)
plot_iq(1e3*t_reg, v)
plt.title("VM output")

plt.xlabel("t [ms]")
plt.show()







plt.show()

#llrf_board.disconnect()
# FIXME: Check factors of two in the downconversion

# Consider using scipy.optimize.least_squares for nonlinear least squarers
