### What is this repository for? ###

Code (MatLab to start with) for running control experiments on firmware/IOC

### How do I get set up? ###

Install labca, only need to download files and then add the path to MatLab. [http://www.slac.stanford.edu/~strauman/labca/]

Some useful stuff to add to .bashrc (for LLRF-LION, modulations might be needed)


```
#!bash
export PATH=$PATH:/home/eit_ess/oloft/labca_3_5_0/bin/linux-x86_64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/eit_ess/oloft/labca_3_5_0/lib/linux-x86_64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/epics/bases/base-3.14.12.5/lib/centos7-x86_64
export EPICS_CA_MAX_ARRAY_BYTES=1600000

```
The LD_LIBRARY_PATH to the shared objects (EPICS) was not necessary to start with but apparently it is now.  